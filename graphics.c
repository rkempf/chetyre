/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * graphics.c                                                         *
 **********************************************************************
 *                                                                    *
 * Contains graphics rendering and loading code.                      *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Functions:                                                  *
 *      int init_graphics(void):                                      *
 *          Initializes graphics. Call at beginning.                  *
 *      void load_tiles(void):                                        *
 *          Loads tile graphics for init and swapping.                *
 *      void render(void): Renders the current game state.            *
 *      void clean_tiles(void): Cleans tile graphics.                 *
 *      void clean_graphics(void): Cleans up. Call at end.            *
 *                                                                    *
 * Private Functions:                                                 *
 *      int init_window(void):                                        *
 *          Initializes window. Called at beginning.                  *
 *      int init_renderer(void):                                      *
 *          Inits renderer. Called at beginning.                      *
 *      void load_graphics(void):                                     *
 *          Loads in many textures and sets up rectangles.            *
 *      SDL_Texture *load_texture(char *path):                        *
 *          Loads texture in path.                                    *
 *      void render_gameboard(void):                                  *
 *          Renders the gameboard sans blocks.                        *
 *      void render_blockstate(void):                                 *
 *          Renders blocks on gameboard.                              *
 *      void render_scores(void):                                     *
 *          Renders scores on side of screen.                         *
 *      void render_block(void):                                      *
 *          Renders the current block.                                *
 *      void render_main_menu(void):                                  *
 *          Renders the main menu.                                    *
 *      void render_highscores(void):                                 *
 *          Renders high scores page.                                 *
 *      void render_options_en(void):                                 *
 *      void render_options_ru(void):                                 *
 *          Renders options menu in English or Russian respectively.  *
 *      void render_controls_en(void):                                *
 *      void render_controls_ru(void):                                *
 *          Renders controls menu in English or Russian respectively. *
 *      void render_menu_border(void):                                *
 *          Renders a border of pipes around the screen.              *
 *      SDL_Rect render_string(wchar_t *str,                          *
 *                             int len,                               *
 *                             SDL_Rect dest_rect,                    *
 *                             int nl,                                *
 *                             int grey):                             *
 *          Renders a string to a location.                           *
 *      SDL_Rect render_letter(char c, SDL_Rect dest_rect, int grey): *
 *          Renders a character to a location.                        *
 *                                                                    *
 **********************************************************************/

#include <wchar.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "graphics.h"
#include "log.h"
#include "field.h"
#include "block.h"
#include "score.h"
#include "game.h"
#include "menu.h"
#include "options.h"
#include "controls.h"

#define SPRITE_HEIGHT 16
#define SPRITE_WIDTH 16
#define SPRITES_ACROSS 24
#define SPRITES_UPDOWN 24

int init_window(void);
int init_renderer(void);
void load_graphics(void);
SDL_Texture *load_texture(char *path);
void render_gameboard(void);
void render_blockstate(void);
void render_scores(void);
void render_block(struct block block);
void render_menu_border(void);
void render_main_menu(void);
void render_highscores(void);
void render_options_en(void);
void render_options_ru(void);
void render_controls_en(void);
void render_controls_ru(void);
SDL_Rect render_string(wchar_t *str, int len, SDL_Rect dest_rect, int nl, int grey);
SDL_Rect render_letter(wchar_t c, SDL_Rect dest_rect, int grey);

SDL_Window *window;
SDL_Renderer *renderer;
int graphics_loaded = 0;

// Basic pipe images to surround regions like play region.
SDL_Texture *pipe_top_left;
SDL_Texture *pipe_top_right;
SDL_Texture *pipe_bottom_left;
SDL_Texture *pipe_bottom_right;
SDL_Texture *pipe_horiz;
SDL_Texture *pipe_vert;

// These pipes have words on them.
SDL_Texture *bigpipe_score;
SDL_Texture *bigpipe_high;
SDL_Texture *bigpipe_next;
SDL_Texture *bigpipe_schyot;
SDL_Texture *bigpipe_rekord;
SDL_Texture *bigpipe_sled;

// Images for each of the 7 tetraminos, the ghosts, and the plain grid.
SDL_Texture *tiles[9];
// Images for each digit 0-9.
SDL_Texture *score_nums;

// Flash when a row is scored.
SDL_Texture *flash;

// Main menu title screen
SDL_Texture *title;

// Alphabets
SDL_Texture *alphabet_roman;
SDL_Texture *alphabet_cyrillic;
SDL_Texture *numbers;
SDL_Texture *alphabet_roman_grey;
SDL_Texture *alphabet_cyrillic_grey;
SDL_Texture *numbers_grey;

// Selection indicators for menus
SDL_Texture *selection;

// Destination Rectangle for loading images. Note
// that destR is an array of 16x16 tiles.
SDL_Rect destR[SPRITES_UPDOWN][SPRITES_ACROSS];

/*
 * Function:    init_graphics
 * --------------------------
 *
 * Initializes the graphics "engine".
 *
 * Returns:
 *      ret (int) : Success or failure.
 *
 */

int init_graphics(void)
{
    int flags = IMG_INIT_PNG;
    int initted;
    int ret = 0;

    if (graphics_loaded) return 0;

    initted = IMG_Init(flags);
    if((initted&flags) != flags) {
        printf("IMG_Init: Failed to init required jpg and png support!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
        ret = 1;
    }
    ret |= init_window();
    ret |= init_renderer();
    load_graphics();
    return ret;
}

/*
 * Function:    init_window
 * ------------------------
 *
 * Sets up the SDL_Window.
 *
 * Returns:
 *      (int) : Success or failure.
 *
 * Effects:
 *      window
 *
 */

int init_window(void)
{
    int size = selected_options[OPTION_SIZE] + 1;
    int i;
    int j;

    window = SDL_CreateWindow("Chetyre",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              SPRITE_WIDTH * SPRITES_ACROSS * size,
                              SPRITE_HEIGHT * SPRITES_UPDOWN * size,
                              0);

    for(i = 0; i < (SPRITES_UPDOWN); i++)
    {
        for(j = 0; j < (SPRITES_ACROSS); j++)
        {
            destR[i][j].h = SPRITE_HEIGHT * size;
            destR[i][j].w = SPRITE_WIDTH * size;
            destR[i][j].x = i * SPRITE_HEIGHT * size;
            destR[i][j].y = j * SPRITE_WIDTH * size;
        }
    }

    if (window)
    {
        debug_log(LOG_OUTPUT, "Window Created!");
        return 0;
    }
    else
    {
        return 1;
    }
}

/*
 * Function:    init_renderer
 * --------------------------
 *
 * Sets up the SDL_Renderer.
 *
 * Returns:
 *      (int) : Success or failure.
 *
 * Effects:
 *      renderer
 *
 */

int init_renderer(void)
{
    renderer = SDL_CreateRenderer(window, -1, 0);
    if (renderer)
    {
        SDL_SetRenderDrawColor(renderer, 68, 36, 52, 255);
        debug_log(LOG_OUTPUT, "Renderer Created!");
        return 0;
    }
    else
    {
        return 1;
    }
}

/*
 * Function:    load_graphics
 * --------------------------
 *
 * Loads in all of the graphics. There are many. Also sets up destR array.
 *
 * Effects:
 *      pipe_*
 *      bigpipe_*
 *      score_nums[*]
 *      destR[*][*]
 *
 */

void load_graphics(void)
{
    pipe_bottom_left = load_texture("assets/pipe_bottom_left.png");
    pipe_bottom_right = load_texture("assets/pipe_bottom_right.png");
    pipe_top_left = load_texture("assets/pipe_top_left.png");
    pipe_top_right = load_texture("assets/pipe_top_right.png");
    pipe_horiz = load_texture("assets/pipe_horiz.png");
    pipe_vert = load_texture("assets/pipe_vert.png");

    bigpipe_score = load_texture("assets/bigpipe_score_16x16_5x1.png");
    bigpipe_high = load_texture("assets/bigpipe_high_16x16_4x1.png");
    bigpipe_next = load_texture("assets/bigpipe_next_16x16_4x1.png");

    bigpipe_schyot = load_texture("assets/bigpipe_schyot_16x16_4x1.png");
    bigpipe_rekord = load_texture("assets/bigpipe_rekord_16x16_6x1.png");
    bigpipe_sled = load_texture("assets/bigpipe_sled_16x16_4x1.png");

    load_tiles();

    flash = load_texture("assets/flash.png");

    score_nums = load_texture("assets/score_numbers_16x16_10x1.png");

    title = load_texture("assets/chetyre.png");
    alphabet_roman = load_texture("assets/letters_6x10_13x2.png");
    alphabet_cyrillic = load_texture("assets/cyrillic_6x10_8x4.png");
    numbers = load_texture("assets/numbers_6x10_5x2.png");
    alphabet_roman_grey = load_texture("assets/letters_grey_6x10_13x2.png");
    alphabet_cyrillic_grey = load_texture("assets/cyrillic_grey_6x10_8x4.png");
    numbers_grey = load_texture("assets/numbers_grey_6x10_5x2.png");
    selection = load_texture("assets/menu_selection_5x5_2x2.png");
}

/*
 * Function:    load_tiles
 * -----------------------
 *
 * Loads in tile graphics. Separated out so graphics can be swapped.
 *
 * Effects:
 *      tiles[*]
 *
 */

void load_tiles(void)
{
    if (selected_options[OPTION_GRID] == GRID_ON)
    {
        tiles[TILE_NONE] = load_texture("assets/tile_grid.png");
    }
    else
    {
        tiles[TILE_NONE] = load_texture("assets/tile_none.png");
    }

    if (selected_options[OPTION_BLOCKCOLORS] == BLOCKCOLORS_ON)
    {
        tiles[TILE_BLUE] = load_texture("assets/tile_blue_j.png");
        tiles[TILE_CYAN] = load_texture("assets/tile_cyan_i.png");
        tiles[TILE_GREEN] = load_texture("assets/tile_green_s.png");
        tiles[TILE_ORANGE] = load_texture("assets/tile_orange_l.png");
        tiles[TILE_PURPLE] = load_texture("assets/tile_purple_t.png");
        tiles[TILE_RED] = load_texture("assets/tile_red_z.png");
        tiles[TILE_YELLOW] = load_texture("assets/tile_yellow_o.png");
    }
    else
    {
        tiles[TILE_BLUE] = load_texture("assets/tile_grey.png");
        tiles[TILE_CYAN] = load_texture("assets/tile_grey.png");
        tiles[TILE_GREEN] = load_texture("assets/tile_grey.png");
        tiles[TILE_ORANGE] = load_texture("assets/tile_grey.png");
        tiles[TILE_PURPLE] = load_texture("assets/tile_grey.png");
        tiles[TILE_RED] = load_texture("assets/tile_grey.png");
        tiles[TILE_YELLOW] = load_texture("assets/tile_grey.png");
    }

    if (selected_options[OPTION_GHOSTS] == GHOSTS_ON)
    {
        tiles[TILE_GHOST] = load_texture("assets/tile_ghost.png");
    }
    else
    {
        tiles[TILE_GHOST] = load_texture("assets/tile_none.png");
    }
}

/*
 * Function:    load_texture
 * --------------------------
 *
 * Given a filename, creates a surface and a texture from that surface
 * in order to load it into the game properly.
 *
 * Arguments:
 *      char* path: The path to the file to load.
 *
 * Returns:
 *      SDL_Texture* new_texture: The texture created from path.
 *
 */

SDL_Texture *load_texture(char *path)
{
    SDL_Texture *new_texture = 0;

    SDL_Surface *tmp_surface = IMG_Load(path);
    if (tmp_surface == 0)
    {
        debug_log(LOG_ERROR, "Unable to load surface from %s!");
    }
    else
    {
        new_texture = SDL_CreateTextureFromSurface(renderer, tmp_surface);
        if (new_texture == 0)
        {
            debug_log(LOG_ERROR, "Can't create texture from %s!");
        }

        SDL_FreeSurface(tmp_surface);
    }

    return new_texture;
}

/*
 * Function:    render
 * -------------------
 *
 * Manages all other render functions: depending on the current
 * gamestate, determines what other functions to call to render
 * the appropriate images on the screen.
 * 
 */

void render(void)
{
    SDL_RenderClear(renderer);
    if (is_running)
    {
        if (is_running & GAMESTATE_IS_ACTIVE)
        {
            render_gameboard();
            if (is_running & GAMESTATE_IS_UNPAUSED)
            {
                render_blockstate();
                if (is_running & GAMESTATE_IS_GAMEOVER)
                {
                    // render_gameover_screen();
                }
            }
            else
            {
                // render_pause_menu();
            }
            render_scores();
        }
        else
        {
            if (is_running & GAMESTATE_IS_CONTROLS)
            {
                if (selected_options[OPTION_LANGUAGE] == LANGUAGE_RUSSIAN)
                {
                    render_controls_ru();
                }
                else
                {
                    render_controls_en();
                }
            }
            else if (is_running & GAMESTATE_IS_OPTIONS)
            {
                if (selected_options[OPTION_LANGUAGE] == LANGUAGE_RUSSIAN)
                {
                    render_options_ru();
                }
                else
                {
                    render_options_en();
                }
            }
            else if (is_running & GAMESTATE_IS_HIGHSCORE)
            {
                render_highscores();
            }
            else
            {
                render_main_menu();
            }
        }
    }
    // Finally, we're done!
    SDL_RenderPresent(renderer);
}

/*
 * Function:    render_gameboard
 * -----------------------------
 *
 * Renders the basic gameboard (sans blocks) to the screen.
 * Layers 2-3 on the below chart.
 *
 * Layer 1: The background. (See init_renderer() for this.)
 * Layer 2: The pipes as borders.
 * Layer 3: Buttons. (Currently unimplemented.)
 * Layer 4: Tilegrid and fallen/confirmed blocks.
 * Layer 5: The active and currently falling block (current_block).
 * Layer 6: The ghost block for current_block, if applicable.
 * Layer 7: Scoreboards. (High Score and next_block not implemented.)
 * Layer 8: Pause menus. (Currently unimplemented.)
 *
 */

void render_gameboard(void)
{
    int i = 0;
    int j = 0;
    SDL_Rect src_rect;

    src_rect.w = SPRITE_WIDTH;
    src_rect.h = SPRITE_HEIGHT;

    // add stuff to render - layer by layer
    // layer 2: the pipes
    // pipes: main section
    SDL_RenderCopy(renderer, pipe_top_left, 0, &destR[1][1]);
    SDL_RenderCopy(renderer, pipe_top_right, 0, &destR[12][1]);
    SDL_RenderCopy(renderer, pipe_bottom_left, 0, &destR[1][22]);
    SDL_RenderCopy(renderer, pipe_bottom_right, 0, &destR[12][22]);
    for (j=2; j < 22; j++)
    {
        SDL_RenderCopy(renderer, pipe_vert, 0, &destR[1][j]);
        SDL_RenderCopy(renderer, pipe_vert, 0, &destR[12][j]);
    }
    for (i=2; i < 12; i++)
    {
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[i][1]);
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[i][22]);
    }

    // pipes: score section
    SDL_RenderCopy(renderer, pipe_top_left, 0, &destR[15][1]);
    SDL_RenderCopy(renderer, pipe_top_right, 0, &destR[22][1]);
    SDL_RenderCopy(renderer, pipe_bottom_left, 0, &destR[15][3]);
    SDL_RenderCopy(renderer, pipe_bottom_right, 0, &destR[22][3]);
    SDL_RenderCopy(renderer, pipe_vert, 0, &destR[15][2]);
    SDL_RenderCopy(renderer, pipe_vert, 0, &destR[22][2]);
    if (selected_options[OPTION_LANGUAGE] == LANGUAGE_RUSSIAN)
    {
        src_rect.y = 0;
        for (i=16; i < 20; i++)
        {
            src_rect.x = (i - 16) * SPRITE_WIDTH;
            SDL_RenderCopy(renderer,
                           bigpipe_schyot,
                           &src_rect,
                           &destR[i][1]);
        }
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[20][1]);
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[21][1]);
    }
    else
    {
        src_rect.y = 0;
        for (i=16; i < 21; i++)
        {
            src_rect.x = (i - 16) * SPRITE_WIDTH;
            SDL_RenderCopy(renderer,
                           bigpipe_score,
                           &src_rect,
                           &destR[i][1]);
        }
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[21][1]);
    }

    // pipes: high score
    SDL_RenderCopy(renderer, pipe_top_left, 0, &destR[15][5]);
    SDL_RenderCopy(renderer, pipe_top_right, 0, &destR[22][5]);
    SDL_RenderCopy(renderer, pipe_bottom_left, 0, &destR[15][7]);
    SDL_RenderCopy(renderer, pipe_bottom_right, 0, &destR[22][7]);
    SDL_RenderCopy(renderer, pipe_vert, 0, &destR[15][6]);
    SDL_RenderCopy(renderer, pipe_vert, 0, &destR[22][6]);
    if (selected_options[OPTION_LANGUAGE] == LANGUAGE_RUSSIAN)
    {
        src_rect.y = 0;
        for (i=16; i < 22; i++)
        {
            src_rect.x = (i - 16) * SPRITE_WIDTH;
            SDL_RenderCopy(renderer,
                           bigpipe_rekord,
                           &src_rect,
                           &destR[i][5]);
        }
    }
    else
    {
        src_rect.y = 0;
        for (i=16; i < 20; i++)
        {
            src_rect.x = (i - 16) * SPRITE_WIDTH;
            SDL_RenderCopy(renderer,
                           bigpipe_high,
                           &src_rect,
                           &destR[i][5]);
        }
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[20][5]);
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[21][5]);
    }

    // pipes: next piece
    SDL_RenderCopy(renderer, pipe_top_left, 0, &destR[15][9]);
    SDL_RenderCopy(renderer, pipe_top_right, 0, &destR[22][9]);
    SDL_RenderCopy(renderer, pipe_bottom_left, 0, &destR[15][14]);
    SDL_RenderCopy(renderer, pipe_bottom_right, 0, &destR[22][14]);
    for (j=10; j<14; j++)
    {
        SDL_RenderCopy(renderer, pipe_vert, 0, &destR[15][j]);
        SDL_RenderCopy(renderer, pipe_vert, 0, &destR[22][j]);
    }
    if (selected_options[OPTION_LANGUAGE] == LANGUAGE_RUSSIAN)
    {
        src_rect.y = 0;
        for (i=16; i < 20; i++)
        {
            src_rect.x = (i - 16) * SPRITE_WIDTH;
            SDL_RenderCopy(renderer,
                           bigpipe_sled,
                           &src_rect,
                           &destR[i][9]);
        }
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[20][9]);
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[21][9]);
    }
    else
    {
        src_rect.y = 0;
        for (i=16; i < 20; i++)
        {
            src_rect.x = (i - 16) * SPRITE_WIDTH;
            SDL_RenderCopy(renderer,
                           bigpipe_next,
                           &src_rect,
                           &destR[i][9]);
        }
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[20][9]);
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[21][9]);
    }

    // bottom pipes for convenience
    for (i=16; i < 22; i++)
    {
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[i][3]);
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[i][7]);
        SDL_RenderCopy(renderer, pipe_horiz, 0, &destR[i][14]);
    }

    // layer 3: buttons?
    // this is a low priority for now



    // layer 8: menu
}

/*
 * Function:    render_blockstate
 * ------------------------------
 *
 * Renders the current field, fallen blocks, and the current 
 * and upcoming blocks (as well as the current block's ghost).
 * Layers 4-6 on the below chart.
 *
 * Layer 1: The background. (See init_renderer() for this.)
 * Layer 2: The pipes as borders.
 * Layer 3: Buttons. (Currently unimplemented.)
 * Layer 4: Tilegrid and fallen/confirmed blocks.
 * Layer 5: The active and currently falling block (current_block).
 * Layer 6: The ghost block for current_block, if applicable.
 * Layer 7: Scoreboards. (High Score and next_block not implemented.)
 * Layer 8: Pause menus. (Currently unimplemented.)
 *
 */

void render_blockstate(void)
{
    int i = 0;
    int j = 0;

    // layer 4: tilegrid + fallen blocks
    for (i = 0; i < FIELD_WIDTH; i++)
    {
        for (j = BUFFER_HEIGHT; j < FIELD_HEIGHT+BUFFER_HEIGHT; j++)
        {
            SDL_RenderCopy(renderer, tiles[field[i][j]], 0, &destR[i+2][j+2-BUFFER_HEIGHT]);
        }
    }

    // layer 5: falling blocks + layer 6: ghost block
    render_block(get_block_final_dest(current_block));
    render_block(current_block);
    if (selected_options[OPTION_NEXTBLOCK] == NEXTBLOCK_ON)
    {
        render_block(next_block);
    }
}

/*
 * Function:    render_scores
 * --------------------------
 *
 * Renders the scoreboards. See score.c/score.h for more details.
 * Layer 7 on the below chart.
 *
 * Layer 1: The background. (See init_renderer() for this.)
 * Layer 2: The pipes as borders.
 * Layer 3: Buttons. (Currently unimplemented.)
 * Layer 4: Tilegrid and fallen/confirmed blocks.
 * Layer 5: The active and currently falling block (current_block).
 * Layer 6: The ghost block for current_block, if applicable.
 * Layer 7: Scoreboards. (High Score and next_block not implemented.)
 * Layer 8: Pause menus. (Currently unimplemented.)
 *
 */

void render_scores(void)
{
    int i = 0;
    int score_digits[6] = {0, 0, 0, 0, 0, 0};
    int highscore_digits[6] = {0, 0, 0, 0, 0, 0};
    SDL_Rect src_rect;

    src_rect.w = SPRITE_WIDTH;
    src_rect.h = SPRITE_HEIGHT;
    src_rect.y = 0;

    // layer 7: score
    score_digits[5] = current_score % 10;
    score_digits[4] = (current_score / 10) % 10;
    score_digits[3] = (current_score / 100) % 10;
    score_digits[2] = (current_score / 1000) % 10;
    score_digits[1] = (current_score / 10000) % 10;
    score_digits[0] = (current_score / 100000) % 10;
    if (highscore != NULL)
    {
        highscore_digits[5] = highscore->value % 10;
        highscore_digits[4] = (highscore->value / 10) % 10;
        highscore_digits[3] = (highscore->value / 100) % 10;
        highscore_digits[2] = (highscore->value / 1000) % 10;
        highscore_digits[1] = (highscore->value / 10000) % 10;
        highscore_digits[0] = (highscore->value / 100000) % 10;
    }
    for (i = 0; i < 6; i++)
    {
        src_rect.x = score_digits[i]*SPRITE_WIDTH;
        SDL_RenderCopy(renderer,
                       score_nums,
                       &src_rect,
                       &destR[i+16][2]);
        if ((highscore == NULL) || (highscore->value > current_score))
        {
            src_rect.x = highscore_digits[i]*SPRITE_WIDTH;
            SDL_RenderCopy(renderer,
                           score_nums,
                           &src_rect,
                           &destR[i+16][6]);
        }
        else
        {
            src_rect.x = score_digits[i]*SPRITE_WIDTH;
            SDL_RenderCopy(renderer,
                           score_nums,
                           &src_rect,
                           &destR[i+16][6]);
        }
    }
}

/*
 * Function:    render_block
 * -------------------------
 *
 * Given a (struct) block, renders it in the appropriate location
 * and with the appropriate images.
 *
 * Arguments:
 *      struct block block: The block to render.
 *
 */

void render_block(struct block block)
{
    int i = 0;

    for (i = 0; i < 4; i++)
    {
        //printf("i=%d, x=%d, y=%d\n", i, block.x[i], block.y[i]);
        if (block.y[i] >= BUFFER_HEIGHT)
        {
            SDL_RenderCopy(renderer,
                           tiles[block.type],
                           0,
                           &destR[block.x[i]+2]
                                 [block.y[i]+2-BUFFER_HEIGHT]);
        }
    }
}

/*
 * Function:    render_main_menu
 * -----------------------------
 *
 * Renders the main menu, including selected object.
 *
 */

void render_main_menu(void)
{
    int size = selected_options[OPTION_SIZE] + 1;
    SDL_Rect dest_rect;
    SDL_Rect src_rect;

    // border
    // render_menu_border();

    // title
    dest_rect.w = 332 * size;
    dest_rect.h = 84 * size;
    dest_rect.x = ((SPRITE_HEIGHT * SPRITES_UPDOWN) - 332) * size / 2;
    dest_rect.y = 20 * size;
    SDL_RenderCopy(renderer, title, 0, &dest_rect);

    // options
    dest_rect.w = 6 * size;
    dest_rect.h = 10 * size;
    dest_rect.x = 144 * size;
    dest_rect.y = 200 * size;
    if (selected_options[OPTION_LANGUAGE] == LANGUAGE_ENGLISH)
    {
        dest_rect = render_string(L"NEW GAME", 8, dest_rect, 1, 0);
        dest_rect = render_string(L"HIGH SCORES", 11, dest_rect, 1, 0);
        dest_rect = render_string(L"OPTIONS", 7, dest_rect, 1, 0);
        dest_rect = render_string(L"CONTROLS", 8, dest_rect, 1, 0);
        dest_rect = render_string(L"QUIT", 4, dest_rect, 1, 0);
    }
    else
    {
        dest_rect = render_string(L"ИГРАТЬ", 6, dest_rect, 1, 0);
        dest_rect = render_string(L"РЕКОРДЫ", 7, dest_rect, 1, 0);
        dest_rect = render_string(L"НАСТРОЙКИ", 9, dest_rect, 1, 0);
        dest_rect = render_string(L"УПРАВЛЕНИЯ", 10, dest_rect, 1, 0);
        dest_rect = render_string(L"УЙТИ", 4, dest_rect, 1, 0);
    }

    // selected option
    dest_rect.x = 140 * size;
    dest_rect.y = ((196 * size) + (dest_rect.h * 2 * main_menu_selected));
    dest_rect.w = 5 * size;
    dest_rect.h = 5 * size;
    src_rect.x = 0;
    src_rect.y = 0;
    src_rect.w = 5;
    src_rect.h = 5;
    SDL_RenderCopy(renderer, selection, &src_rect, &dest_rect);
    dest_rect.x += ((8 * 11) + 1) * size;
    src_rect.x = 5;
    src_rect.y = 0;
    SDL_RenderCopy(renderer, selection, &src_rect, &dest_rect);
    dest_rect.y += (10 + 3) * size;
    src_rect.x = 5;
    src_rect.y = 5;
    SDL_RenderCopy(renderer, selection, &src_rect, &dest_rect);
    dest_rect.x -= ((8 * 11) + 1) * size;
    src_rect.x = 0;
    src_rect.y = 5;
    SDL_RenderCopy(renderer, selection, &src_rect, &dest_rect);
}

/*
 * Function:    render_highscores
 * ------------------------------
 *
 * Renders the high scores screen.
 *
 */

void render_highscores(void)
{
    SDL_Rect dest_rect;
    int i;
    int size = selected_options[OPTION_SIZE] + 1;
    wchar_t score_string[10];
    struct score *index_score = highscore;

    // border
    // render_menu_border();

    // title
    dest_rect.w = 332 * size;
    dest_rect.h = 84 * size;
    dest_rect.x = ((SPRITE_HEIGHT * SPRITES_UPDOWN) - 332) * size / 2;
    dest_rect.y = 20 * size;
    SDL_RenderCopy(renderer, title, 0, &dest_rect);

    // subtitle
    dest_rect.w = 6 * size;
    dest_rect.h = 10 * size;
    dest_rect.x = 144 * size;
    dest_rect.y = 145 * size;
    if (selected_options[OPTION_LANGUAGE] == LANGUAGE_RUSSIAN)
    {
        dest_rect = render_string(L"  РЕКОРДЫ", 9, dest_rect, 1, 0);
    }
    else
    {
        dest_rect = render_string(L"HIGH SCORES", 11, dest_rect, 1, 0);
    }
    dest_rect.x += 8 * size;

    for (i=1; i <= 10; i++)
    {
        if (index_score)
        {
            swprintf(score_string, 10, L"%-2d %6d", i, index_score->value);
            dest_rect = render_string(score_string, 11, dest_rect, 1, 0);
            index_score = index_score->next;
        }
    }
}

/*
 * Function:    render_options_en
 * ------------------------------
 *
 * Renders the options screen in English.
 *
 */

void render_options_en(void)
{
    SDL_Rect dest_rect;
    SDL_Rect curs_rect;
    SDL_Rect src_rect;
    int size = selected_options[OPTION_SIZE] + 1;
    int ix_option;
    int ix_suboption;
    wchar_t *options_strings[13] =
    {
        L"BLOCK CHOICE",
        L"GRID",
        L"GHOSTS",
        L"NEXT BLOCK",
        L"BLOCK COLORS",
        L"STARTING LEVEL",
        L"SIZE",
        L"MUSIC",
        L"MUSIC VOLUME",
        L"LANGUAGE",
        L"",
        L"CLEAR HIGH SCORES",
        L"BACK TO MAIN MENU"
    };
    int options_lens[13] = {12, 4, 6, 10, 12, 14, 4, 5, 12, 8, 0, 17, 17};
    wchar_t *subopts_strings[10][10] =
    {
        {
            L"GRABBAG  ",
            L"RANDOM  ",
            L"SADISTIC",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"GRID     ",
            L"NO GRID",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"GHOSTS   ",
            L"NO GHOSTS",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"NEXT     ",
            L"NO NEXT  ",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"COLORS   ",
            L"GREYSCALE",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"",
            L"1 ",
            L"2 ",
            L"3 ",
            L"4 ",
            L"5 ",
            L"6 ",
            L"7 ",
            L"8 ",
            L"9 "
        },
        {
            L"384X384  ",
            L"768X768",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"KOROBEINIKI ",
            L"KATYUSHA ",
            L"NONE",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"",
            L"1 ",
            L"2 ",
            L"3 ",
            L"4 ",
            L"5 ",
            L"6 ",
            L"7 ",
            L"8 ",
            L"9 "
        },
        {
            L"ENGLISH  ",
            L"RUSSIAN",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        }
    };
    int subopts_lengths[10][2][10] =
    {
        {
            {9, 8, 8, 0, 0, 0, 0, 0, 0, 0},
            {7, 6, 8, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {9, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            {4, 7, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {9, 9, 0, 0, 0, 0, 0, 0, 0, 0},
            {6, 9, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {9, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            {4, 7, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {9, 9, 0, 0, 0, 0, 0, 0, 0, 0},
            {6, 9, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {0, 2, 2, 2, 2, 2, 2, 2, 2, 1},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        },
        {
            {9, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            {7, 7, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {12, 9, 4, 0, 0, 0, 0, 0, 0, 0},
            {11, 8, 4, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {0, 2, 2, 2, 2, 2, 2, 2, 2, 1},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        },
        {
            {9, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            {7, 7, 0, 0, 0, 0, 0, 0, 0, 0}
        }
    };

    // render the border
    render_menu_border();

    // render the title
    dest_rect.w = 6 * size;
    dest_rect.h = 10 * size;
    dest_rect.x = 20 * size;
    dest_rect.y = 32 * size;
    dest_rect = render_string(L"OPTIONS", 7, dest_rect, 1, 0);

    dest_rect = render_string(L"", 0, dest_rect, 1, 0);
    dest_rect = render_string(L"", 0, dest_rect, 1, 0);

    // render the available options
    for (ix_option = 0; ix_option < 13; ix_option++)
    {
        if (options_menu_selected == ix_option)
        {
            if ((options_menu_selected == OPTION_CLEARHIGHSCORES) |
                (options_menu_selected == OPTION_BACKTOMAINMENU))
            {
                // render cursor
                curs_rect.w = 5 * size;
                curs_rect.h = 5 * size;
                curs_rect.x = dest_rect.x - 4 * size;
                curs_rect.y = dest_rect.y - 4 * size;
                src_rect.x = 0;
                src_rect.y = 0;
                src_rect.w = 5;
                src_rect.h = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.x += ((8 * options_lens[ix_option]) + 1) * size;
                src_rect.x = 5;
                src_rect.y = 0;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.y += ((10 + 3) * size);
                src_rect.x = 5;
                src_rect.y = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.x -= ((8 * options_lens[ix_option]) + 1) * size;
                src_rect.x = 0;
                src_rect.y = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
            }
            dest_rect = render_string(options_strings[ix_option],
                                      options_lens[ix_option],
                                      dest_rect,
                                      1,
                                      0);
        }
        else
        {
            dest_rect = render_string(options_strings[ix_option],
                                      options_lens[ix_option],
                                      dest_rect,
                                      1,
                                      1);
        }
    }

    dest_rect.x = 140 * size;
    dest_rect.y = 92 * size;

    for (ix_option = 0; ix_option < 10; ix_option++)
    {
        for (ix_suboption = min_options[ix_option];
             ix_suboption <= max_options[ix_option];
             ix_suboption++)
        {
            if ((options_menu_selected == ix_option) &&
                (selected_options[ix_option] == ix_suboption))
            {
                // render cursor
                curs_rect.w = 5 * size;
                curs_rect.h = 5 * size;
                curs_rect.x = dest_rect.x - 4 * size;
                curs_rect.y = dest_rect.y - 4 * size;
                src_rect.x = 0;
                src_rect.y = 0;
                src_rect.w = 5;
                src_rect.h = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.x += ((8 * subopts_lengths[ix_option]
                                                    [1]
                                                    [ix_suboption]) + 1)
                                * size;
                src_rect.x = 5;
                src_rect.y = 0;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.y += ((10 + 3) * size);
                src_rect.x = 5;
                src_rect.y = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.x -= ((8 * subopts_lengths[ix_option]
                                                    [1]
                                                    [ix_suboption]) + 1)
                                * size;
                src_rect.x = 0;
                src_rect.y = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                
                // render word
                dest_rect = render_string(subopts_strings[ix_option]
                                                         [ix_suboption],
                                          subopts_lengths[ix_option]
                                                         [0]
                                                         [ix_suboption],
                                          dest_rect,
                                          0,
                                          0);
            }
            else
            {
                // render word
                dest_rect = render_string(subopts_strings[ix_option]
                                                         [ix_suboption],
                                          subopts_lengths[ix_option]
                                                         [0]
                                                         [ix_suboption],
                                          dest_rect,
                                          0,
                                          1);
            }
        }

        dest_rect = render_string(L"", 0, dest_rect, 1, 0);
        dest_rect.x = 140 * size;
    }
}

/*
 * Function:    render_options_ru
 * ------------------------------
 *
 * Renders the options screen in Russian.
 *
 */

void render_options_ru(void)
{
    SDL_Rect dest_rect;
    SDL_Rect curs_rect;
    SDL_Rect src_rect;
    int size = selected_options[OPTION_SIZE] + 1;
    int ix_option;
    int ix_suboption;
    wchar_t *options_strings[13] =
    {
        L"ВЫБОР БЛОКОВ",
        L"СЕТКА",
        L"ТЕНИ",
        L"СЛЕДУЮЩИЙ БЛОК",
        L"ЦВЕТА БЛОКОВ",
        L"СТАРТОВЫЙ УРОВЕНЬ",
        L"РАЗМЕР ОКНА",
        L"МУЗЫКА",
        L"ГРОМКОСТЬ МУЗЫКИ",
        L"ЯЗЫК",
        L"",
        L"УДАЛИТЬ РЕКОРДЫ",
        L"ВЕРНУТЬСЯ К ГЛАВНОМУ МЕНЮ"
    };
    int options_lens[13] = {12, 5, 4, 14, 12, 17, 11, 6, 16, 4, 0, 15, 25};
    wchar_t *subopts_strings[10][10] =
    {
        {
            L"ПЕРЕМЕШАТЬ ",
            L"СЛУЧАЙНЫЙ ",
            L"ЗЛОЙ",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"СЕТКА      ",
            L"НЕТ СЕТКЕ",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"ТЕНИ       ",
            L"НЕТ ТЕНЕЙ",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"СЛЕД       ",
            L"НЕТ СЛЕД",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"ЦВЕТНАЯ    ",
            L"ОТТЕНКИ СЕРОГО",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"",
            L"1 ",
            L"2 ",
            L"3 ",
            L"4 ",
            L"5 ",
            L"6 ",
            L"7 ",
            L"8 ",
            L"9"
        },
        {
            L"384X384    ",
            L"768X768",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"КОРОБЕЙНИКИ ",
            L"КАТЮША ",
            L"ВЫКЛ",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        },
        {
            L"",
            L"1 ",
            L"2 ",
            L"3 ",
            L"4 ",
            L"5 ",
            L"6 ",
            L"7 ",
            L"8 ",
            L"9"
        },
        {
            L"АНГЛИЙСКИЙ ",
            L"РУССКИЙ",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L"",
            L""
        }
    };
    int subopts_lengths[10][2][10] =
    {
        {
            {11, 10, 4, 0, 0, 0, 0, 0, 0, 0},
            {10,  9, 4, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {11, 9, 0, 0, 0, 0, 0, 0, 0, 0},
            { 5, 9, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {11, 9, 0, 0, 0, 0, 0, 0, 0, 0},
            { 4, 9, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {11, 8, 0, 0, 0, 0, 0, 0, 0, 0},
            { 4, 8, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {11, 14, 0, 0, 0, 0, 0, 0, 0, 0},
            { 7, 14, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {0, 2, 2, 2, 2, 2, 2, 2, 2, 1},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        },
        {
            {11, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            { 7, 7, 0, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {12, 7, 4, 0, 0, 0, 0, 0, 0, 0},
            {11, 6, 4, 0, 0, 0, 0, 0, 0, 0}
        },
        {
            {0, 2, 2, 2, 2, 2, 2, 2, 2, 1},
            {0, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        },
        {
            {11, 7, 0, 0, 0, 0, 0, 0, 0, 0},
            {10, 7, 0, 0, 0, 0, 0, 0, 0, 0}
        }
    };

    // render the border
    render_menu_border();

    // render the title
    dest_rect.w = 6 * size;
    dest_rect.h = 10 * size;
    dest_rect.x = 20 * size;
    dest_rect.y = 32 * size;
    dest_rect = render_string(L"НАСТРОЙКИ", 9, dest_rect, 1, 0);

    dest_rect = render_string(L"", 0, dest_rect, 1, 0);
    dest_rect = render_string(L"", 0, dest_rect, 1, 0);

    // render the available options
    for (ix_option = 0; ix_option < 13; ix_option++)
    {
        if (options_menu_selected == ix_option)
        {
            if ((options_menu_selected == OPTION_CLEARHIGHSCORES) |
                (options_menu_selected == OPTION_BACKTOMAINMENU))
            {
                // render cursor
                curs_rect.w = 5 * size;
                curs_rect.h = 5 * size;
                curs_rect.x = dest_rect.x - 4 * size;
                curs_rect.y = dest_rect.y - 4 * size;
                src_rect.x = 0;
                src_rect.y = 0;
                src_rect.w = 5;
                src_rect.h = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.x += ((8 * options_lens[ix_option]) + 1) * size;
                src_rect.x = 5;
                src_rect.y = 0;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.y += (10 + 3) * size;
                src_rect.x = 5;
                src_rect.y = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.x -= ((8 * options_lens[ix_option]) + 1) * size;
                src_rect.x = 0;
                src_rect.y = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
            }
            dest_rect = render_string(options_strings[ix_option],
                                      options_lens[ix_option],
                                      dest_rect,
                                      1,
                                      0);
        }
        else
        {
            dest_rect = render_string(options_strings[ix_option],
                                      options_lens[ix_option],
                                      dest_rect,
                                      1,
                                      1);
        }
    }

    dest_rect.x = 164 * size;
    dest_rect.y = 92 * size;

    for (ix_option = 0; ix_option < 10; ix_option++)
    {
        for (ix_suboption = min_options[ix_option];
             ix_suboption <= max_options[ix_option];
             ix_suboption++)
        {
            if ((options_menu_selected == ix_option) &&
                (selected_options[ix_option] == ix_suboption))
            {
                // render cursor
                curs_rect.w = 5 * size;
                curs_rect.h = 5 * size;
                curs_rect.x = dest_rect.x - 4 * size;
                curs_rect.y = dest_rect.y - 4 * size;
                src_rect.x = 0;
                src_rect.y = 0;
                src_rect.w = 5;
                src_rect.h = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.x += ((8 * subopts_lengths[ix_option]
                                                    [1]
                                                    [ix_suboption]) + 1)
                                * size;
                src_rect.x = 5;
                src_rect.y = 0;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.y += (10 + 3) * size;
                src_rect.x = 5;
                src_rect.y = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                curs_rect.x -= ((8 * subopts_lengths[ix_option]
                                                    [1]
                                                    [ix_suboption]) + 1)
                                * size;
                src_rect.x = 0;
                src_rect.y = 5;
                SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
                
                // render word
                dest_rect = render_string(subopts_strings[ix_option]
                                                         [ix_suboption],
                                          subopts_lengths[ix_option]
                                                         [0]
                                                         [ix_suboption],
                                          dest_rect,
                                          0,
                                          0);
            }
            else
            {
                // render word
                dest_rect = render_string(subopts_strings[ix_option]
                                                         [ix_suboption],
                                          subopts_lengths[ix_option]
                                                         [0]
                                                         [ix_suboption],
                                          dest_rect,
                                          0,
                                          1);
            }
        }

        dest_rect = render_string(L"", 0, dest_rect, 1, 0);
        dest_rect.x = 164 * size;
    }
}

/*
 * Function:    render_controls_en
 * -------------------------------
 *
 * Renders the controls screen in English.
 *
 */

void render_controls_en(void)
{
    SDL_Rect dest_rect;
    SDL_Rect curs_rect;
    SDL_Rect src_rect;
    int size = selected_options[OPTION_SIZE] + 1;
    int ix_key;
    enum game_event ge;
    SDL_Scancode sc;
    wchar_t *controls_strings[13] =
    {
        L"QUIT",
        L"LEFT",
        L"RIGHT",
        L"UP",
        L"DOWN",
        L"CCW",
        L"CANCEL",
        L"CONFIRM",
        L"",
        L"",
        L"",
        L"RESET KEYBINDINGS",
        L"BACK TO MAIN MENU"
    };
    int controls_lens[13] = {4, 4, 5, 2, 4, 3, 6, 7, 0, 0, 0, 17, 17};

    // render the border
    render_menu_border();

    // render the title
    dest_rect.w = 6 * size;
    dest_rect.h = 10 * size;
    dest_rect.x = 20 * size;
    dest_rect.y = 32 * size;
    dest_rect = render_string(L"CONTROLS", 8, dest_rect, 1, 0);

    dest_rect = render_string(L"", 0, dest_rect, 1, 0);
    dest_rect = render_string(L"", 0, dest_rect, 1, 0);

    // render the available options
    for (ix_key = CONTROLS_MIN; ix_key <= CONTROLS_MAX; ix_key++)
    {
        if (controls_menu_selected == ix_key)
        {
            // render cursor
            curs_rect.w = 5 * size;
            curs_rect.h = 5 * size;
            curs_rect.x = dest_rect.x - 4 * size;
            curs_rect.y = dest_rect.y - 4 * size;
            src_rect.x = 0;
            src_rect.y = 0;
            src_rect.w = 5;
            src_rect.h = 5;
            SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
            curs_rect.x += ((8 * controls_lens[ix_key]) + 1) * size;
            src_rect.x = 5;
            src_rect.y = 0;
            SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
            curs_rect.y += ((10 + 3) * size);
            src_rect.x = 5;
            src_rect.y = 5;
            SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
            curs_rect.x -= ((8 * controls_lens[ix_key]) + 1) * size;
            src_rect.x = 0;
            src_rect.y = 5;
            SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);

            // Render control name
            dest_rect = render_string(controls_strings[ix_key],
                                      controls_lens[ix_key],
                                      dest_rect,
                                      1,
                                      0);
        }
        else
        {
            dest_rect = render_string(controls_strings[ix_key],
                                      controls_lens[ix_key],
                                      dest_rect,
                                      1,
                                      1);
        }
    }

    dest_rect.x = 140 * size;
    dest_rect.y = 92 * size;

    for (ix_key = 0; ix_key < 8; ix_key++)
    {
        ge = events_from_controls[ix_key];
        sc = keys_from_events[ge];
        if (controls_menu_selected == ix_key)
        {
            if ((waiting >= 0) &&
                (events_from_controls[waiting] == ge))
            {
                dest_rect = render_string(L"WAITING FOR INPUT",
                                          17,
                                          dest_rect,
                                          1,
                                          0);
            }
            else
            {
                dest_rect = render_string(strings_from_keys[sc],
                                          strlen_from_keys[sc],
                                          dest_rect,
                                          1,
                                          0);
            }
        }
        else
        {
            dest_rect = render_string(strings_from_keys[sc],
                                      strlen_from_keys[sc],
                                      dest_rect,
                                      1,
                                      1);
        }
    }
}

/*
 * Function:    render_controls_ru
 * -------------------------------
 *
 * Renders the controls screen in Russian.
 *
 */

void render_controls_ru(void)
{
    SDL_Rect dest_rect;
    SDL_Rect curs_rect;
    SDL_Rect src_rect;
    int size = selected_options[OPTION_SIZE] + 1;
    int ix_key;
    enum game_event ge;
    SDL_Scancode sc;
    wchar_t *controls_strings[13] =
    {
        L"УЙТИ",
        L"ЛЕВЫЙ",
        L"ПРАВЫЙ",
        L"ВВЕРХ",
        L"ВНИЗ",
        L"ПЧС",
        L"ОТМЕНЫ",
        L"ВЫБОРА",
        L"",
        L"",
        L"",
        L"ПЕРЕЗАГРУЗИТЬ КОМБИНАЦИИ",
        L"ВЕРНУТЬСЯ К ГЛАВНОМУ МЕНЮ"
    };
    int controls_lens[13] = {4, 5, 6, 5, 4, 3, 6, 6, 0, 0, 0, 24, 25};

    // render the border
    render_menu_border();

    // render the title
    dest_rect.w = 6 * size;
    dest_rect.h = 10 * size;
    dest_rect.x = 20 * size;
    dest_rect.y = 32 * size;
    dest_rect = render_string(L"УПРАВЛЕНИЯ", 10, dest_rect, 1, 0);

    dest_rect = render_string(L"", 0, dest_rect, 1, 0);
    dest_rect = render_string(L"", 0, dest_rect, 1, 0);

    // render the available options
    for (ix_key = CONTROLS_MIN; ix_key <= CONTROLS_MAX; ix_key++)
    {
        if (controls_menu_selected == ix_key)
        {
            // render cursor
            curs_rect.w = 5 * size;
            curs_rect.h = 5 * size;
            curs_rect.x = dest_rect.x - 4 * size;
            curs_rect.y = dest_rect.y - 4 * size;
            src_rect.x = 0;
            src_rect.y = 0;
            src_rect.w = 5;
            src_rect.h = 5;
            SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
            curs_rect.x += ((8 * controls_lens[ix_key]) + 1) * size;
            src_rect.x = 5;
            src_rect.y = 0;
            SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
            curs_rect.y += ((10 + 3) * size);
            src_rect.x = 5;
            src_rect.y = 5;
            SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);
            curs_rect.x -= ((8 * controls_lens[ix_key]) + 1) * size;
            src_rect.x = 0;
            src_rect.y = 5;
            SDL_RenderCopy(renderer, selection, &src_rect, &curs_rect);

            // Render control name
            dest_rect = render_string(controls_strings[ix_key],
                                      controls_lens[ix_key],
                                      dest_rect,
                                      1,
                                      0);
        }
        else
        {
            dest_rect = render_string(controls_strings[ix_key],
                                      controls_lens[ix_key],
                                      dest_rect,
                                      1,
                                      1);
        }
    }

    dest_rect.x = 140 * size;
    dest_rect.y = 92 * size;

    for (ix_key = 0; ix_key < 8; ix_key++)
    {
        ge = events_from_controls[ix_key];
        sc = keys_from_events[ge];
        if (controls_menu_selected == ix_key)
        {
            if ((waiting >= 0) &&
                (events_from_controls[waiting] == ge))
            {
                dest_rect = render_string(L"WAITING FOR INPUT",
                                          17,
                                          dest_rect,
                                          1,
                                          0);
            }
            else
            {
                dest_rect = render_string(strings_from_keys[sc],
                                          strlen_from_keys[sc],
                                          dest_rect,
                                          1,
                                          0);
            }
        }
        else
        {
            dest_rect = render_string(strings_from_keys[sc],
                                      strlen_from_keys[sc],
                                      dest_rect,
                                      1,
                                      1);
        }
    }
}

/*
 * Function:    render_menu_border
 * -------------------------------
 *
 * Renders a border of pipes around the entire screen.
 *
 */

void render_menu_border(void)
{
    int i;

    SDL_RenderCopy(renderer,
                   pipe_top_left,
                   0,
                   &destR[0][0]);
    SDL_RenderCopy(renderer,
                   pipe_top_right,
                   0,
                   &destR[SPRITES_UPDOWN-1][0]);
    SDL_RenderCopy(renderer,
                   pipe_bottom_left,
                   0,
                   &destR[0][SPRITES_ACROSS-1]);
    SDL_RenderCopy(renderer,
                   pipe_bottom_right,
                   0,
                   &destR[SPRITES_ACROSS-1][SPRITES_UPDOWN-1]);

    for (i = 1; i < SPRITES_ACROSS - 1; i++)
    {
        SDL_RenderCopy(renderer,
                       pipe_horiz,
                       0,
                       &destR[i][0]);
        SDL_RenderCopy(renderer,
                       pipe_horiz,
                       0,
                       &destR[i][SPRITES_UPDOWN-1]);
    }

    for (i = 1; i < SPRITES_UPDOWN - 1; i++)
    {
        SDL_RenderCopy(renderer,
                       pipe_vert,
                       0,
                       &destR[0][i]);
        SDL_RenderCopy(renderer,
                       pipe_vert,
                       0,
                       &destR[SPRITES_ACROSS-1][i]);
    }
}

/*
 * Function:    render_string
 * --------------------------
 *
 * Given a string, its length, and a location, renders a string
 * (consisting of numbers, Roman letters, spaces, and Cyrillic
 *  characters only).
 *
 * Arguments:
 *      char* c: The character string to render.
 *      int len: The length of the string, with or without \0.
 *      SDL_Rect dest_rect: The location of the inital letter.
 *      int nl: Whether or not to put a new line at the end.
 *      int grey: If true, uses grey font instead of white.
 *
 * Returns:
 *      SDL_Rect return_rect: The next initial location. If nl
 *          is true, will be on following line; otherwise,
 *          will be the location immediately following.
 *
 */

SDL_Rect render_string(wchar_t *str,
                       int len,
                       SDL_Rect dest_rect,
                       int nl,
                       int grey)
{
    int i = 0;
    SDL_Rect return_rect = dest_rect;
    if (nl)
    {
        return_rect.y = dest_rect.y + (dest_rect.h * 2);
    }

    for (i = 0; i < len; i++)
    {
        dest_rect = render_letter(str[i], dest_rect, grey);
    }

    if (nl == 0)
    {
        return_rect = dest_rect;
    }

    return return_rect;
}

/*
 * Function:    render_letter
 * --------------------------
 *
 * Renders an alphabetic character at a given location.
 *
 * Arguments:
 *      char c: The character to render.
 *      SDL_Rect dest_rect: The location of the letter.
 *      int grey: If true, uses grey font instead of white.
 *
 * Returns:
 *      SDL_Rect return_rect: The assumed next letter's location.
 *
 */

SDL_Rect render_letter(wchar_t c, SDL_Rect dest_rect, int grey)
{
    SDL_Rect src_rect;
    SDL_Rect return_rect = dest_rect;
    int xpos = c;
    int ypos = 0;
    return_rect.x = dest_rect.x + ((dest_rect.w * 4) / 3);
    src_rect.w = 6;
    src_rect.h = 10;

    if ((c >= L'0') && (c <= L'9'))
    {
        // Number
        xpos = c - L'0';
        if (xpos >= 5)
        {
            xpos -= 5;
            ypos = 1;
        }
        src_rect.x = xpos * 6;
        src_rect.y = ypos * 10;
        if (grey)
        {
            SDL_RenderCopy(renderer,
                           numbers_grey,
                           &src_rect,
                           &dest_rect);
        }
        else
        {
            SDL_RenderCopy(renderer,
                           numbers,
                           &src_rect,
                           &dest_rect);
        }
    }
    else if ((c >= L'a') && (c <= L'z'))
    {
        // Roman Lowercase
        xpos = c - L'a';
        if (xpos >= 13)
        {
            xpos -= 13;
            ypos = 1;
        }
        src_rect.x = xpos * 6;
        src_rect.y = ypos * 10;
        if (grey)
        {
            SDL_RenderCopy(renderer,
                           alphabet_roman_grey,
                           &src_rect,
                           &dest_rect);
        }
        else
        {
            SDL_RenderCopy(renderer,
                           alphabet_roman,
                           &src_rect,
                           &dest_rect);
        }
    }
    else if ((c >= L'A') && (c <= L'Z'))
    {
        // Roman Uppercase
        xpos = c - L'A';
        if (xpos >= 13)
        {
            xpos -= 13;
            ypos = 1;
        }
        src_rect.x = xpos * 6;
        src_rect.y = ypos * 10;
        if (grey)
        {
            SDL_RenderCopy(renderer,
                           alphabet_roman_grey,
                           &src_rect,
                           &dest_rect);
        }
        else
        {
            SDL_RenderCopy(renderer,
                           alphabet_roman,
                           &src_rect,
                           &dest_rect);
        }
    }
    else if ((c >= L'\x0430') && (c <= L'\x044f'))
    {
        // Cyrillic Lowercase
        xpos = c - L'\x0430';
        ypos = xpos / 8;
        xpos = xpos - (8 * ypos);
        src_rect.x = xpos * 6;
        src_rect.y = ypos * 10;
        if (grey)
        {
            SDL_RenderCopy(renderer,
                           alphabet_cyrillic_grey,
                           &src_rect,
                           &dest_rect);
        }
        else
        {
            SDL_RenderCopy(renderer,
                           alphabet_cyrillic,
                           &src_rect,
                           &dest_rect);
        }
    }
    else if ((c >= L'\x0410') && (c <= L'\x042f'))
    {
        // Cyrillic Uppercase
        xpos = c - L'\x0410';
        ypos = xpos / 8;
        xpos = xpos - (ypos * 8);
        src_rect.x = xpos * 6;
        src_rect.y = ypos * 10;
        if (grey)
        {
            SDL_RenderCopy(renderer,
                           alphabet_cyrillic_grey,
                           &src_rect,
                           &dest_rect);
        }
        else
        {
            SDL_RenderCopy(renderer,
                           alphabet_cyrillic,
                           &src_rect,
                           &dest_rect);
        }
    }
    else if ((c == L'\x0451') || (c == L'\x0401'))
    {
        // Cyrillic letter yo is displayed as ye
        xpos = 5;
        ypos = 0;
        src_rect.x = xpos * 6;
        src_rect.y = ypos * 10;

        if (grey)
        {
            SDL_RenderCopy(renderer,
                           alphabet_cyrillic_grey,
                           &src_rect,
                           &dest_rect);
        }
        else
        {
            SDL_RenderCopy(renderer,
                           alphabet_cyrillic,
                           &src_rect,
                           &dest_rect);
        }
    }

    return return_rect;
}

/*
 * Function:    clean_tiles
 * ---------------------------
 *
 * Cleans up tile graphics.
 *
 * Effects:
 *      tiles
 *
 */

void clean_tiles(void)
{
    int i;

    for (i = TILE_NONE; i<TILE_GHOST; i++)
    {
        SDL_DestroyTexture(tiles[i]);
    }
}

/*
 * Function:    clean_graphics
 * ---------------------------
 *
 * Cleans up graphics when game is completed. Destroys window
 * and renderer.
 *
 * Effects:
 *      window
 *      renderer
 *      all textures
 *
 */

void clean_graphics(void)
{
    printf("Cleaning graphics.\n");
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_DestroyTexture(pipe_bottom_left);
    SDL_DestroyTexture(pipe_bottom_right);
    SDL_DestroyTexture(pipe_top_left);
    SDL_DestroyTexture(pipe_top_right);
    SDL_DestroyTexture(pipe_horiz);
    SDL_DestroyTexture(pipe_vert);

    SDL_DestroyTexture(bigpipe_score);
    SDL_DestroyTexture(bigpipe_high);
    SDL_DestroyTexture(bigpipe_next);
    SDL_DestroyTexture(bigpipe_schyot);
    SDL_DestroyTexture(bigpipe_rekord);
    SDL_DestroyTexture(bigpipe_sled);

    clean_tiles();

    SDL_DestroyTexture(flash);

    SDL_DestroyTexture(score_nums);

    SDL_DestroyTexture(title);
    SDL_DestroyTexture(alphabet_roman);
    SDL_DestroyTexture(alphabet_cyrillic);
    SDL_DestroyTexture(numbers);
    SDL_DestroyTexture(alphabet_roman_grey);
    SDL_DestroyTexture(alphabet_cyrillic_grey);
    SDL_DestroyTexture(numbers_grey);
    SDL_DestroyTexture(selection);
}
