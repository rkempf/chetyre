/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2020 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * options.h                                                          *
 **********************************************************************
 *                                                                    *
 * Governs options menu logic.                                        *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      int[13] selected_options: Current options choices.            *
 *      int[13] min_options: Minimum value for each option.           *
 *      int[13] max_options: Maximum value for each option.           *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_options(void):                                      *
 *          Initializes options with default values.                  *
 *      void setup_options_menu(void):                                *
 *          Sets up initial options menu state.                       *
 *      void change_value_current_option(int direction):              *
 *          Changes the value of the current option.                  *
 *      void change_current_option(int direction):                    *
 *          Changes the currently selected option.                    *
 *      void save_options(void):                                      *
 *          Saves options to a file.                                  *
 *      void load_options(void):                                      *
 *          Loads options from a file.                                *
 *                                                                    *
 * Private Functions:                                                 *
 *      void set_value_option(enum option option, int value):         *
 *          Sets the value of option to value.                        *
 *                                                                    *
 **********************************************************************/

#include <stdio.h>
#include "options.h"
#include "menu.h"
#include "music.h"
#include "graphics.h"
#include "score.h"
#include "game.h"

int selected_options[13];
int min_options[13]; // min val of each option
int max_options[13]; // max val of each option

void set_value_option(enum option option, int value);

/*
 * Function:    init_options
 * -------------------------
 *
 * Sets up the initial state of selected options.
 *
 * Effects:
 *      min_options
 *      max_options
 *      selected_options
 *
 */

void init_options(void)
{
    // minimum values for each option
    min_options[OPTION_BLOCKCHOICE] = 0;
    min_options[OPTION_GRID] = 0;
    min_options[OPTION_GHOSTS] = 0;
    min_options[OPTION_NEXTBLOCK] = 0;
    min_options[OPTION_BLOCKCOLORS] = 0;
    min_options[OPTION_STARTINGLEVEL] = 1;
    min_options[OPTION_SIZE] = 0;
    min_options[OPTION_MUSIC] = 0;
    min_options[OPTION_MUSICVOLUME] = 1;
    min_options[OPTION_LANGUAGE] = 0;
    min_options[10] = -1;
    min_options[OPTION_CLEARHIGHSCORES] = 0;
    min_options[OPTION_BACKTOMAINMENU] = 0;

    // maximum values for each option
    max_options[OPTION_BLOCKCHOICE] = 2;
    max_options[OPTION_GRID] = 1;
    max_options[OPTION_GHOSTS] = 1;
    max_options[OPTION_NEXTBLOCK] = 1;
    max_options[OPTION_BLOCKCOLORS] = 1;
    max_options[OPTION_STARTINGLEVEL] = 9;
    max_options[OPTION_SIZE] = 1;
    max_options[OPTION_MUSIC] = 2;
    max_options[OPTION_MUSICVOLUME] = 9;
    max_options[OPTION_LANGUAGE] = 1;
    max_options[10] = -1;
    max_options[OPTION_CLEARHIGHSCORES] = 0;
    max_options[OPTION_BACKTOMAINMENU] = 0;

    // default values
    set_value_option(OPTION_BLOCKCHOICE, BLOCKCHOICE_RANDOM);
    set_value_option(OPTION_GRID, GRID_ON);
    set_value_option(OPTION_GHOSTS, GHOSTS_ON);
    set_value_option(OPTION_NEXTBLOCK, NEXTBLOCK_ON);
    set_value_option(OPTION_BLOCKCOLORS, BLOCKCOLORS_ON);
    set_value_option(OPTION_STARTINGLEVEL, 1);
    set_value_option(OPTION_SIZE, SIZE_384);
    set_value_option(OPTION_MUSIC, MUSIC_KOROBEINIKI);
    set_value_option(OPTION_MUSICVOLUME, 5);
    set_value_option(OPTION_LANGUAGE, LANGUAGE_ENGLISH);
    selected_options[10] = -1;
    selected_options[11] = 0;
    selected_options[12] = 0;

    // load saved settings from file
    load_options();
}

/*
 * Function:    setup_options_menu
 * -------------------------------
 *
 * Called on load of the options menu to set up initial state.
 *
 * Effects:
 *      options_menu_selected
 *
 */

void setup_options_menu(void)
{
    options_menu_selected = OPTION_BLOCKCHOICE;
}

/*
 * Function:    set_value_option
 * -----------------------------
 *
 * Sets the value of a given option and calls relevant functions.
 *
 * Arguments:
 *      enum option option: Which option to set the value of.
 *      int value: value to (attempt to) set option to.
 *
 * Effects:
 *      selected_options
 *
 */

void set_value_option(enum option option, int value)
{
    int old = selected_options[option];

    selected_options[option] = value;

    if (selected_options[option] > max_options[option])
    {
        selected_options[option] = max_options[option];
    }

    if (selected_options[option] < min_options[option])
    {
        selected_options[option] = min_options[option];
    }

    if (selected_options[option] == old) return;

    switch (option)
    {
        case OPTION_BLOCKCHOICE:
            break;
        case OPTION_GRID:
            clean_tiles();
            load_tiles();
            break;
        case OPTION_GHOSTS:
            clean_tiles();
            load_tiles();
            break;
        case OPTION_NEXTBLOCK:
            break;
        case OPTION_BLOCKCOLORS:
            clean_tiles();
            load_tiles();
            break;
        case OPTION_STARTINGLEVEL:
            break;
        case OPTION_SIZE:
            clean_graphics();
            init_graphics();
            break;
        case OPTION_MUSIC:
            if (value == 0)
            {
                switch_music(SONG_KOROBEINIKI);
                play_music();
            }
            else if (value == 1)
            {
                switch_music(SONG_KATYUSHA);
                play_music();
            }
            else
            {
                switch_music(SONG_NONE);
            }
            break;
        case OPTION_MUSICVOLUME:
            set_music_volume(selected_options[option]);
            break;
        case OPTION_LANGUAGE:
            break;
        default:
            break;
    };

    printf("Setting option %d to %d.\n", option, selected_options[option]);
}

/*
 * Function:    change_value_current_option
 * ----------------------------------------
 *
 * Changes the value of the currently selected option.
 *
 * Arguments:
 *      int direction: Positive -> right, negative -> left.
 *
 * Effects:
 *      selected_options
 *
 */

void change_value_current_option(int direction)
{
    int dir;
    if (direction > 0)
    {
        dir = 1;
    }
    else if (direction < 0)
    {
        dir = -1;
    }
    else
    {
        return;
    }

    set_value_option(options_menu_selected,
                     selected_options[options_menu_selected]+dir);
}

/*
 * Function:    change_current_option
 * ----------------------------------
 *
 * Changes which option is currently selected by the cursor.
 *
 * Arguments:
 *      int direction: Positive -> down, negative -> up.
 *
 * Effects:
 *      options_menu_selected
 *
 */

void change_current_option(int direction)
{
    int dir;
    int curr;
    if (direction > 0)
    {
        dir = 1;
    }
    else if (direction < 0)
    {
        dir = -1;
    }
    else
    {
        return;
    }

    // find next (this is horribly squirrelly code...)
    curr = options_menu_selected + dir;
    while (1)
    {
        if ((selected_options[curr] >= 0) &
            (curr <= OPTION_MAX) &
            (curr >= OPTION_MIN))
        {
            options_menu_selected = curr;
            break;
        }
        else if ((curr > OPTION_MAX) | (curr < OPTION_MIN))
        {
            break;
        }
        curr += dir;
    }

    printf("Direction: %d; dir: %d\n", direction, dir);
    printf("Setting current option to: %d\n", options_menu_selected);
}

/*
 * Function:    save_options
 * -------------------------
 *
 * Saves options to a file.
 *
 * Effects:
 *      options.txt
 *
 */

void save_options(void)
{
    FILE *fp;

    fp = fopen("options.txt", "w");

    fprintf(fp, "Block Choice: %d\n", selected_options[OPTION_BLOCKCHOICE]);
    fprintf(fp, "Grid: %d\n", selected_options[OPTION_GRID]);
    fprintf(fp, "Ghosts: %d\n", selected_options[OPTION_GHOSTS]);
    fprintf(fp, "Next Block: %d\n", selected_options[OPTION_NEXTBLOCK]);
    fprintf(fp, "Block Colors: %d\n", selected_options[OPTION_BLOCKCOLORS]);
    fprintf(fp, "Starting Level: %d\n", selected_options[OPTION_STARTINGLEVEL]);
    fprintf(fp, "Size: %d\n", selected_options[OPTION_SIZE]);
    fprintf(fp, "Music: %d\n", selected_options[OPTION_MUSIC]);
    fprintf(fp, "Music Volume: %d\n", selected_options[OPTION_MUSICVOLUME]);
    fprintf(fp, "Language: %d\n", selected_options[OPTION_LANGUAGE]);

    fclose(fp);
}

/*
 * Function:    load_options
 * -------------------------
 *
 * Loads options from a file.
 *
 * Effects:
 *      selected_options
 *
 */

void load_options(void)
{
    FILE *fp;
    int items_read;
    int data;

    fp = fopen("options.txt", "r");
    if (fp == NULL) return;

    items_read = fscanf(fp, "Block Choice: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_BLOCKCHOICE, data);
    items_read = fscanf(fp, "Grid: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_GRID, data);
    items_read = fscanf(fp, "Ghosts: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_GHOSTS, data);
    items_read = fscanf(fp, "Next Block: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_NEXTBLOCK, data);
    items_read = fscanf(fp, "Block Colors: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_BLOCKCOLORS, data);
    items_read = fscanf(fp, "Starting Level: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_STARTINGLEVEL, data);
    items_read = fscanf(fp, "Size: %d\n", &data);
    if (items_read > 0) selected_options[OPTION_SIZE] = data;
    items_read = fscanf(fp, "Music: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_MUSIC, data);
    items_read = fscanf(fp, "Music Volume: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_MUSICVOLUME, data);
    items_read = fscanf(fp, "Language: %d\n", &data);
    if (items_read > 0) set_value_option(OPTION_LANGUAGE, data);

    fclose(fp);
}
