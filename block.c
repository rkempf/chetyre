/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * events.c                                                           *
 **********************************************************************
 *                                                                    *
 * Contains logic to handle events and key presses and delegate them  *
 * to the appropriate locations.                                      *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      struct block current_block: The currently active block.       *
 *      struct block next_block: The next block in the queue.         *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_block(void):                                        *
 *          Initializes the block system.                             *
 *      void update_block(void):                                      *
 *          Updates the current block.                                *
 *      struct block get_block_final_dest(struct block current):      *
 *          Gets the final destination for the current block.         *
 *      struct block attempt_move(struct block curr,                  *
 *                                enum direction dir):                *
 *          Attempts move and returns result.                         *
 *      struct block attempt_rotate(struct block block, int ccw):     *
 *          Attempts a rotation and returns result.                   *
 *      int block_is_equal(struct block block_1,                      *
 *                         struct block block_2):                     *
 *          Checks whether two blocks are equal or not.               *
 *                                                                    *
 * Private Functions:                                                 *
 *      void confirm_block(void):                                     *
 *          Puts block from current_block into the field.             *
 *      void init_prototypes(void):                                   *
 *          Initializes block prototypes for copying.                 *
 *      int can_place_block(struct block block):                      *
 *          Whether a block placement is valid or not.                *
 *      void fprint_block(FILE *stream, struct block block):          *
 *          Prints block information to a filestream.                 *
 *      void print_block(struct block block):                         *
 *          Prints block information to stdout.                       *
 *      int tile_has_block(int row, int col):                         *
 *          Returns whether a given field tile has a block.           *
 *      int i_allowed(void):                                          *
 *      int j_allowed(void):                                          *
 *      int l_allowed(void):                                          *
 *      int o_allowed(void):                                          *
 *      int t_allowed(void):                                          *
 *          Returns whether or not a specific tile is desired.        *
 *      struct block attempt_rotate_sub(struct block block, int ccw): *
 *          Sub-function for rotation, used in attempt_rotate.        *
 *      void set_next_block(void):                                    *
 *          Sets the current and next blocks.                         *
 *                                                                    *
 **********************************************************************/

#include <inttypes.h>
#include <SDL2/SDL.h>
#include "block.h"
#include "field.h"
#include "log.h"
#include "score.h"
#include "game.h"
#include "options.h"

void confirm_block(void);
void init_prototypes(void);
int can_place_block(struct block block);
void fprint_block(FILE *stream, struct block block);
void print_block(struct block block);
int tile_has_block(int row, int col);
int i_allowed(void);
int j_allowed(void);
int l_allowed(void);
int o_allowed(void);
int t_allowed(void);
struct block attempt_rotate_sub(struct block block, int ccw);
void set_next_block(void);

// The current, active block.
struct block current_block;
// The upcoming block, next in the queue.
struct block next_block;
// Array of the seven blocks, ordered, with an array index.
struct block grabbag[7];
int grabbag_index;
// An array of prototypes for the seven block types (1-7) and a null
// block prototype (0). Used for copying into next_block and current_block.
struct block protos[8];
// Time since the last "fall" move.
uint32_t last_move;
// 1 if the block was confirmed at the time of the last "fall" move
// and therefore the next_block needs moved into current_block;
// 0 if the current_block is presently active.
int block_confirmed;
// Only allow floor kicks once per block
int has_floor_kicked;

/*
 * Function:    init_block
 * -----------------------
 *
 * Initialization step which sets up values of initial parameters
 * relating to blocks.
 *
 * Effects:
 *      current_block
 *      next_block
 *      last_move
 *      block_confirmed
 *
 */

void init_block(void)
{
    int i = 0;
    struct block tmp_block;
    int swap_index;

    init_prototypes();
    current_block = protos[TILE_PURPLE];
    next_block = protos[TILE_YELLOW];
    for (i = 0; i < 4; i++)
    {
        next_block.x[i] += 12;
        next_block.y[i] += 9 + BUFFER_HEIGHT;
    }

    last_move = SDL_GetTicks();
    block_confirmed = 0;

    for (grabbag_index = 0; grabbag_index < 7; grabbag_index++)
    {
        grabbag[grabbag_index] = protos[grabbag_index+1];
    }
    for (grabbag_index = 7; grabbag_index > 0; grabbag_index--)
    {
        tmp_block = grabbag[grabbag_index-1];
        swap_index = (rand() % grabbag_index);
        grabbag[grabbag_index-1] = grabbag[swap_index];
        grabbag[swap_index] = tmp_block;
    }
    grabbag_index = 0;
}

/*
 * Function:    init_prototypes
 * ----------------------------
 *
 * Sets up prototypes for the seven blocks as well as an unblock.
 *
 * Effects:
 *      protos
 *
 */

void init_prototypes(void)
{
    // TILE_NONE / NULL BLOCK
    protos[TILE_NONE].x[0] = 0;
    protos[TILE_NONE].y[0] = 0;

    protos[TILE_NONE].x[1] = 0;
    protos[TILE_NONE].y[1] = 0;

    protos[TILE_NONE].x[2] = 0;
    protos[TILE_NONE].y[2] = 0;

    protos[TILE_NONE].x[3] = 0;
    protos[TILE_NONE].y[3] = 0;

    protos[TILE_NONE].orientation = ORIENT_NORTH;
    protos[TILE_NONE].type = TILE_NONE;

    // TILE_BLUE / J
    protos[TILE_BLUE].x[0] = 3;
    protos[TILE_BLUE].y[0] = 0;

    protos[TILE_BLUE].x[1] = 3;
    protos[TILE_BLUE].y[1] = 1;

    protos[TILE_BLUE].x[2] = 4;
    protos[TILE_BLUE].y[2] = 1;

    protos[TILE_BLUE].x[3] = 5;
    protos[TILE_BLUE].y[3] = 1;

    protos[TILE_BLUE].orientation = ORIENT_NORTH;
    protos[TILE_BLUE].type = TILE_BLUE;

    // TILE_CYAN / I
    protos[TILE_CYAN].x[0] = 3;
    protos[TILE_CYAN].y[0] = 0;

    protos[TILE_CYAN].x[1] = 4;
    protos[TILE_CYAN].y[1] = 0;

    protos[TILE_CYAN].x[2] = 5;
    protos[TILE_CYAN].y[2] = 0;

    protos[TILE_CYAN].x[3] = 6;
    protos[TILE_CYAN].y[3] = 0;

    protos[TILE_CYAN].orientation = ORIENT_NORTH;
    protos[TILE_CYAN].type = TILE_CYAN;

    // TILE_GREEN / S
    protos[TILE_GREEN].x[0] = 5;
    protos[TILE_GREEN].y[0] = 0;

    protos[TILE_GREEN].x[1] = 4;
    protos[TILE_GREEN].y[1] = 0;

    protos[TILE_GREEN].x[2] = 4;
    protos[TILE_GREEN].y[2] = 1;

    protos[TILE_GREEN].x[3] = 3;
    protos[TILE_GREEN].y[3] = 1;

    protos[TILE_GREEN].orientation = ORIENT_NORTH;
    protos[TILE_GREEN].type = TILE_GREEN;

    // TILE_ORANGE / L
    protos[TILE_ORANGE].x[0] = 5;
    protos[TILE_ORANGE].y[0] = 0;

    protos[TILE_ORANGE].x[1] = 5;
    protos[TILE_ORANGE].y[1] = 1;

    protos[TILE_ORANGE].x[2] = 4;
    protos[TILE_ORANGE].y[2] = 1;

    protos[TILE_ORANGE].x[3] = 3;
    protos[TILE_ORANGE].y[3] = 1;

    protos[TILE_ORANGE].orientation = ORIENT_NORTH;
    protos[TILE_ORANGE].type = TILE_ORANGE;

    // TILE_PURPLE / T
    protos[TILE_PURPLE].x[0] = 3;
    protos[TILE_PURPLE].y[0] = 1;

    protos[TILE_PURPLE].x[1] = 4;
    protos[TILE_PURPLE].y[1] = 0;

    protos[TILE_PURPLE].x[2] = 5;
    protos[TILE_PURPLE].y[2] = 1;

    protos[TILE_PURPLE].x[3] = 4;
    protos[TILE_PURPLE].y[3] = 1;

    protos[TILE_PURPLE].orientation = ORIENT_NORTH;
    protos[TILE_PURPLE].type = TILE_PURPLE;

    // TILE_RED / Z
    protos[TILE_RED].x[0] = 3;
    protos[TILE_RED].y[0] = 0;

    protos[TILE_RED].x[1] = 4;
    protos[TILE_RED].y[1] = 0;

    protos[TILE_RED].x[2] = 4;
    protos[TILE_RED].y[2] = 1;

    protos[TILE_RED].x[3] = 5;
    protos[TILE_RED].y[3] = 1;

    protos[TILE_RED].orientation = ORIENT_NORTH;
    protos[TILE_RED].type = TILE_RED;

    // TILE_YELLOW / O
    protos[TILE_YELLOW].x[0] = 4;
    protos[TILE_YELLOW].y[0] = 0;

    protos[TILE_YELLOW].x[1] = 5;
    protos[TILE_YELLOW].y[1] = 0;

    protos[TILE_YELLOW].x[2] = 5;
    protos[TILE_YELLOW].y[2] = 1;

    protos[TILE_YELLOW].x[3] = 4;
    protos[TILE_YELLOW].y[3] = 1;

    protos[TILE_YELLOW].orientation = ORIENT_NORTH;
    protos[TILE_YELLOW].type = TILE_YELLOW;
}

/*
 * Function:    set_next_block
 * ---------------------------
 *
 * Algorithmically sets which block should appear next in the cue.
 * Choice depends on whether the block choice method is Random (i.e.
 * selection with replacement), Grabbag (i.e. selection without
 * replacement; reshuffles every 7), or Sadistic (as Random, but the
 * available possibilities does not include the blocks you want).
 *
 * Effects:
 *      current_block
 *      next_block
 *
 */

void set_next_block(void)
{
    struct block tmp_block;
    int swap_index;
    int avail_blocks[7];

    has_floor_kicked = 0;

    switch (selected_options[OPTION_BLOCKCHOICE])
    {
        // BLOCKCHOICE_GRABBAG ensures that you don't have a run of
        // any one type of block by arranging the seven possibilities
        // in a random order every seven blocks.
        case BLOCKCHOICE_GRABBAG:
            current_block = protos[next_block.type];
            next_block = protos[grabbag[grabbag_index].type];
            grabbag_index++;
            printf("Grab-bag: Curr=%d, Next=%d\n", current_block.type, next_block.type);
            // if the array is "empty", reset
            if (grabbag_index == 7)
            {
                for (grabbag_index = 7; grabbag_index > 0; grabbag_index--)
                {
                    tmp_block = grabbag[grabbag_index-1];
                    swap_index = (rand() % grabbag_index);
                    grabbag[grabbag_index-1] = grabbag[swap_index];
                    grabbag[swap_index] = tmp_block;
                }
                for (grabbag_index = 0; grabbag_index < 7; grabbag_index++)
                {
                    printf("%d ", grabbag[grabbag_index].type);
                }
                printf("\n");
                grabbag_index = 0;
            }
            break;
        // BLOCKCHOICE_RANDOM chooses a random block every time.
        case BLOCKCHOICE_RANDOM:
            current_block = protos[next_block.type];
            next_block = protos[(rand() % 7) + 1];
            break;
        // BLOCKCHOICE_SADISTIC doesn't choose blocks you want. At
        // present, it's not very sophisticated of an algorithm.
        case BLOCKCHOICE_SADISTIC:
            current_block = protos[next_block.type];
            swap_index = 2;
            avail_blocks[0] = TILE_GREEN; // S Block
            avail_blocks[1] = TILE_RED;   // Z Block
            if (t_allowed() == 1)
            {
                avail_blocks[swap_index] = TILE_PURPLE; // T Block
                swap_index++;
            }
            if (o_allowed() == 1)
            {
                avail_blocks[swap_index] = TILE_YELLOW; // O Block
                swap_index++;
            }
            if (i_allowed() == 1)
            {
                avail_blocks[swap_index] = TILE_CYAN; // I Block
                swap_index++;
            }
            if (l_allowed() == 1)
            {
                avail_blocks[swap_index] = TILE_ORANGE; // L Block
                swap_index++;
            }
            if (j_allowed() == 1)
            {
                avail_blocks[swap_index] = TILE_BLUE; // J Block
                swap_index++;
            }
            next_block = protos[avail_blocks[(rand() % swap_index)]];
            break;
        default:
            current_block = protos[next_block.type];
            next_block = protos[(rand() % 7) + 1];
            break;
    }
}

/*
 * Function:    tile_has_block
 * ---------------------------
 *
 * Determines whether a given field tile has a block or not.
 *
 * Returns:
 *      int tile_has_block: 1 if yes, 0 if no.
 *
 */

int tile_has_block(int row, int col)
{
    if ((field[col][row] == TILE_NONE) | (field[col][row] == TILE_GHOST))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/*
 * Function:    i_allowed
 * ----------------------
 *
 * Determines whether an I block is desired.
 * Desired if:
 *  *_*
 *  X_X
 *  X_X
 *  X_X
 *
 * Returns:
 *      int i_allowed: 1 if yes, 0 if no.
 *
 */

int i_allowed(void)
{
    int row_index = 0;
    int col_index = 0;

    // does the player want an I block?
    // *_* | X_X | X_X | X_X -> yes

    // blocks on either side
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        for (col_index = 0; col_index < FIELD_WIDTH-3; col_index++)
        {
            if ((tile_has_block(row_index, col_index+1) == 0) &

                (tile_has_block(row_index+1, col_index) == 1) &
                (tile_has_block(row_index+1, col_index+1) == 0) &
                (tile_has_block(row_index+1, col_index+2) == 1) &

                (tile_has_block(row_index+2, col_index) == 1) &
                (tile_has_block(row_index+2, col_index+1) == 0) &
                (tile_has_block(row_index+2, col_index+2) == 1) &

                (tile_has_block(row_index+3, col_index) == 1) &
                (tile_has_block(row_index+3, col_index+1) == 0) &
                (tile_has_block(row_index+3, col_index+2) == 1)
               )
            {
                return 0;
            }
        }
    }

    // blocks on right
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        if ((tile_has_block(row_index, 0) == 0) &

            (tile_has_block(row_index+1, 0) == 0) &
            (tile_has_block(row_index+1, 1) == 1) &

            (tile_has_block(row_index+2, 0) == 0) &
            (tile_has_block(row_index+2, 1) == 1) &

            (tile_has_block(row_index+3, 0) == 0) &
            (tile_has_block(row_index+3, 1) == 1)
           )
        {
            return 0;
        }
    }

    // blocks on left
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        if ((tile_has_block(row_index, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+1, FIELD_WIDTH-2) == 1) &
            (tile_has_block(row_index+1, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+2, FIELD_WIDTH-2) == 1) &
            (tile_has_block(row_index+2, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+3, FIELD_WIDTH-2) == 1) &
            (tile_has_block(row_index+3, FIELD_WIDTH-1) == 0)
           )
        {
            return 0;
        }
    }
    return 1;
}

/*
 * Function:    l_allowed
 * ----------------------
 *
 * Determines whether an L block is desired.
 * Desired if:
 *  *__*
 *  X__*
 *  XX_X
 *  XX_X
 *
 * Returns:
 *      int l_allowed: 1 if yes, 0 if no.
 *
 */

int l_allowed(void)
{
    int row_index = 0;
    int col_index = 0;

    // does the player want an L block?
    // *__* | *__* | XX_X | XX_X -> yes

    // blocks on either side
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        for (col_index = 0; col_index < FIELD_WIDTH-4; col_index++)
        {
            if ((tile_has_block(row_index, col_index+1) == 0) &
                (tile_has_block(row_index, col_index+2) == 0) &

                (tile_has_block(row_index+1, col_index+1) == 0) &
                (tile_has_block(row_index+1, col_index+2) == 0) &

                (tile_has_block(row_index+2, col_index) == 1) &
                (tile_has_block(row_index+2, col_index+1) == 1) &
                (tile_has_block(row_index+2, col_index+2) == 0) &
                (tile_has_block(row_index+2, col_index+3) == 1) &

                (tile_has_block(row_index+3, col_index) == 1) &
                (tile_has_block(row_index+3, col_index+1) == 1) &
                (tile_has_block(row_index+3, col_index+2) == 0) &
                (tile_has_block(row_index+3, col_index+3) == 1)
               )
            {
                return 0;
            }
        }
    }

    // blocks on right
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        if ((tile_has_block(row_index, 0) == 0) &
            (tile_has_block(row_index, 1) == 0) &

            (tile_has_block(row_index+1, 0) == 0) &
            (tile_has_block(row_index+1, 1) == 0) &

            (tile_has_block(row_index+2, 0) == 1) &
            (tile_has_block(row_index+2, 1) == 0) &
            (tile_has_block(row_index+2, 2) == 1) &

            (tile_has_block(row_index+3, 0) == 1) &
            (tile_has_block(row_index+3, 1) == 0) &
            (tile_has_block(row_index+3, 2) == 1)
           )
        {
            return 0;
        }
    }

    // blocks on left
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        if ((tile_has_block(row_index, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+1, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index+1, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+2, FIELD_WIDTH-3) == 1) &
            (tile_has_block(row_index+2, FIELD_WIDTH-2) == 1) &
            (tile_has_block(row_index+2, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+3, FIELD_WIDTH-3) == 1) &
            (tile_has_block(row_index+3, FIELD_WIDTH-2) == 1) &
            (tile_has_block(row_index+3, FIELD_WIDTH-1) == 0)
           )
        {
            return 0;
        }
    }
    return 1;
}

/*
 * Function:    j_allowed
 * ----------------------
 *
 * Determines whether a J block is desired.
 * Desired if:
 *  *__*
 *  *__*
 *  X_XX
 *  X_XX
 *
 * Returns:
 *      int j_allowed: 1 if yes, 0 if no.
 *
 */

int j_allowed(void)
{
    int row_index = 0;
    int col_index = 0;

    // does the player want a J block?
    // *__* | *__X | X_XX | X_XX -> yes
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        for (col_index = 0; col_index < FIELD_WIDTH-4; col_index++)
        {
            if ((tile_has_block(row_index, col_index+1) == 0) &
                (tile_has_block(row_index, col_index+2) == 0) &

                (tile_has_block(row_index+1, col_index+1) == 0) &
                (tile_has_block(row_index+1, col_index+2) == 0) &

                (tile_has_block(row_index+2, col_index) == 1) &
                (tile_has_block(row_index+2, col_index+1) == 0) &
                (tile_has_block(row_index+2, col_index+2) == 1) &
                (tile_has_block(row_index+2, col_index+3) == 1) &

                (tile_has_block(row_index+3, col_index) == 1) &
                (tile_has_block(row_index+3, col_index+1) == 0) &
                (tile_has_block(row_index+3, col_index+2) == 1) &
                (tile_has_block(row_index+3, col_index+3) == 1)
               )
            {
                return 0;
            }
        }
    }

    // blocks on right
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        if ((tile_has_block(row_index, 0) == 0) &
            (tile_has_block(row_index, 1) == 0) &

            (tile_has_block(row_index+1, 0) == 0) &
            (tile_has_block(row_index+1, 1) == 0) &

            (tile_has_block(row_index+2, 0) == 0) &
            (tile_has_block(row_index+2, 1) == 1) &
            (tile_has_block(row_index+2, 2) == 1) &

            (tile_has_block(row_index+3, 0) == 0) &
            (tile_has_block(row_index+3, 1) == 1) &
            (tile_has_block(row_index+3, 2) == 1)
           )
        {
            return 0;
        }
    }

    // blocks on left
    for (row_index = 0; row_index < FIELD_HEIGHT-4; row_index++)
    {
        if ((tile_has_block(row_index, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+1, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index+1, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+2, FIELD_WIDTH-3) == 1) &
            (tile_has_block(row_index+2, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index+2, FIELD_WIDTH-1) == 1) &

            (tile_has_block(row_index+3, FIELD_WIDTH-3) == 1) &
            (tile_has_block(row_index+3, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index+3, FIELD_WIDTH-1) == 1)
           )
        {
            return 0;
        }
    }
    return 1;
}

/*
 * Function:    o_allowed
 * ----------------------
 *
 * Determines whether an O block is desired.
 * Desired if:
 *  *__*
 *  X__X
 *  X__X
 *
 * Returns:
 *      int o_allowed: 1 if yes, 0 if no.
 *
 */

int o_allowed(void)
{
    int row_index = 0;
    int col_index = 0;

    // does the player want an O block?
    // *__* | X__X | X__X -> yes

    // blocks on either side
    for (row_index = 0; row_index < FIELD_HEIGHT-3; row_index++)
    {
        for (col_index = 0; col_index < FIELD_WIDTH-4; col_index++)
        {
            if ((tile_has_block(row_index, col_index+1) == 0) &
                (tile_has_block(row_index, col_index+2) == 0) &

                (tile_has_block(row_index+1, col_index) == 1) &
                (tile_has_block(row_index+1, col_index+1) == 0) &
                (tile_has_block(row_index+1, col_index+2) == 0) &
                (tile_has_block(row_index+1, col_index+3) == 1) &

                (tile_has_block(row_index+2, col_index) == 1) &
                (tile_has_block(row_index+2, col_index+1) == 0) &
                (tile_has_block(row_index+2, col_index+2) == 0) &
                (tile_has_block(row_index+2, col_index+3) == 1)
               )
            {
                return 0;
            }
        }
    }

    // blocks on right
    for (row_index = 0; row_index < FIELD_HEIGHT-3; row_index++)
    {
        if ((tile_has_block(row_index, 0) == 0) &
            (tile_has_block(row_index, 1) == 0) &

            (tile_has_block(row_index+1, 0) == 0) &
            (tile_has_block(row_index+1, 1) == 0) &
            (tile_has_block(row_index+1, 2) == 1) &

            (tile_has_block(row_index+2, 0) == 0) &
            (tile_has_block(row_index+2, 1) == 0) &
            (tile_has_block(row_index+2, 2) == 1)
           )
        {
            return 0;
        }
    }

    // blocks on left
    for (row_index = 0; row_index < FIELD_HEIGHT-3; row_index++)
    {
        if ((tile_has_block(row_index, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+1, FIELD_WIDTH-3) == 1) &
            (tile_has_block(row_index+1, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index+1, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+2, FIELD_WIDTH-3) == 1) &
            (tile_has_block(row_index+2, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index+2, FIELD_WIDTH-1) == 0)
           )
        {
            return 0;
        }
    }
    return 1;
}

/*
 * Function:    t_allowed
 * ----------------------
 *
 * Determines whether a T block is desired.
 * Desired if:
 *  *___*
 *  X___X
 *  XX_XX
 *
 * Returns:
 *      int t_allowed: 1 if yes, 0 if no.
 *
 */

int t_allowed(void)
{
    int row_index = 0;
    int col_index = 0;

    // does the player want a T block?
    // *___* | X___X | XX_XX -> yes

    // blocks on either side
    for (row_index = 0; row_index < FIELD_HEIGHT-3; row_index++)
    {
        for (col_index = 0; col_index < FIELD_WIDTH-4; col_index++)
        {
            if ((tile_has_block(row_index, col_index+1) == 0) &
                (tile_has_block(row_index, col_index+2) == 0) &
                (tile_has_block(row_index, col_index+3) == 0) &

                (tile_has_block(row_index+1, col_index) == 1) &
                (tile_has_block(row_index+1, col_index+1) == 0) &
                (tile_has_block(row_index+1, col_index+2) == 0) &
                (tile_has_block(row_index+1, col_index+3) == 0) &
                (tile_has_block(row_index+1, col_index+4) == 1) &

                (tile_has_block(row_index+2, col_index) == 1) &
                (tile_has_block(row_index+2, col_index+1) == 1) &
                (tile_has_block(row_index+2, col_index+2) == 0) &
                (tile_has_block(row_index+2, col_index+3) == 1) &
                (tile_has_block(row_index+2, col_index+4) == 1)
               )
            {
                return 0;
            }
        }
    }

    // blocks on right
    for (row_index = 0; row_index < FIELD_HEIGHT-3; row_index++)
    {
        if ((tile_has_block(row_index, 0) == 0) &
            (tile_has_block(row_index, 1) == 0) &
            (tile_has_block(row_index, 2) == 0) &

            (tile_has_block(row_index+1, 0) == 0) &
            (tile_has_block(row_index+1, 1) == 0) &
            (tile_has_block(row_index+1, 2) == 0) &
            (tile_has_block(row_index+1, 3) == 1) &

            (tile_has_block(row_index+2, 0) == 1) &
            (tile_has_block(row_index+2, 1) == 0) &
            (tile_has_block(row_index+2, 2) == 1) &
            (tile_has_block(row_index+2, 3) == 1)
           )
        {
            return 0;
        }
    }

    // blocks on left
    for (row_index = 0; row_index < FIELD_HEIGHT-3; row_index++)
    {
        if ((tile_has_block(row_index, FIELD_WIDTH-3) == 0) &
            (tile_has_block(row_index, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+1, FIELD_WIDTH-4) == 1) &
            (tile_has_block(row_index+1, FIELD_WIDTH-3) == 0) &
            (tile_has_block(row_index+1, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index+1, FIELD_WIDTH-1) == 0) &

            (tile_has_block(row_index+2, FIELD_WIDTH-4) == 1) &
            (tile_has_block(row_index+2, FIELD_WIDTH-3) == 1) &
            (tile_has_block(row_index+2, FIELD_WIDTH-2) == 0) &
            (tile_has_block(row_index+2, FIELD_WIDTH-1) == 1)
           )
        {
            return 0;
        }
    }
    return 1;
}

/*
 * Function:    update_block
 * -------------------------
 *
 * Updates the current_block. After a certain time, will
 * either get a new block for current_block and next_block,
 * move the current_block, or confirm the current_block.
 *
 * Effects:
 *      current_block
 *      next_block
 *      block_confirmed
 *      last_move
 *
 */

void update_block(void)
{
    int i = 0;
    int blockout = 0;

    if (block_confirmed)
    {
        if (SDL_GetTicks() - last_move > 200)
        {
            set_next_block();
            for (i = 0; i < 4; i++)
            {
                blockout += field[current_block.x[i]][current_block.y[i]];
            }
            for (i = 0; i < 4; i++)
            {
                next_block.x[i] += 12;
                next_block.y[i] += 9 + BUFFER_HEIGHT;
            }
            block_confirmed = 0;
            last_move = SDL_GetTicks();
            if (blockout)
            {
                is_running = GAMESTATE_BLOCKOUT;
            }
        }
    }
    else
    {
        if (SDL_GetTicks() - last_move > get_move_time(level))
        {
            if (block_is_equal(current_block,
                               attempt_move(current_block, DIR_DOWN)))
            {
                confirm_block();
            }
            else
            {
                current_block = attempt_move(current_block, DIR_DOWN);
            }
            last_move = SDL_GetTicks();
        }
    }
}

/*
 * Function:    get_block_final_dest
 * ---------------------------------
 *
 * Gets the current final destination of the block; i.e. where the block
 * would go if it were hard-dropped. This can be used for showing the ghost
 * block and for hard drops.
 *
 * Arguments:
 *      struct block current: the block whose final destination is desired.
 *
 * Returns:
 *      struct block ghost: the final destination as a ghost block.
 *
 */

struct block get_block_final_dest(struct block current)
{
    if (current.type == TILE_NONE) return protos[0];
    struct block ghost = current;
    ghost.type = TILE_GHOST;
    while (!(block_is_equal(attempt_move(ghost, DIR_DOWN), ghost)))
    {
        ghost = attempt_move(ghost, DIR_DOWN);
    }
    return ghost;
}

/*
 * Function:    attempt_move
 * -------------------------
 *
 * Given a block and a direction, gets the result of attempting to move
 * that block in that direction. Does not change the input block. If the
 * block cannot move in that direction, will simply return the given block.
 *
 * Arguments:
 *      struct block curr: The block to attempt to move.
 *      enum direction dir: The direction to move: down, left, right.
 *
 * Returns:
 *      struct block result: The resulting block, moved or not.
 *
 */

struct block attempt_move(struct block curr, enum direction dir)
{
    int i = 0;
    struct block result;

    if (curr.type == TILE_NONE) return curr;

    switch (dir)
    {
        case DIR_DOWN:
            result = curr;
            for (i = 0; i < 4; i++)
            {
                result.y[i] += 1;
            }
            if (can_place_block(result) == 0)
            {
                result = curr;
            }
            break;

        case DIR_LEFT:
            result = curr;
            for (i = 0; i < 4; i++)
            {
                result.x[i] -= 1;
            }
            if (can_place_block(result) == 0)
            {
                result = curr;
            }
            break;

        case DIR_RIGHT:
            result = curr;
            for (i = 0; i < 4; i++)
            {
                result.x[i] += 1;
            }
            if (can_place_block(result) == 0)
            {
                result = curr;
            }
            break;

        // case DIR_UP:
        
        default:
            break;
    }
    return result;
}

/*
 * Function:    block_is_equal
 * ---------------------------
 *
 * Determines whether two blocks have identical data or not.
 *
 * Arguments:
 *      struct block block_1: The first block.
 *      struct block block_2: The second block.
 *
 * Returns:
 *      int result: Whether the two blocks have identical data or not.
 *
 */

int block_is_equal(struct block block_1, struct block block_2)
{
    int result = 1;
    int i = 0;

    for (i = 0; i < 4; i++)
    {
        // TODO: what if they get swapped around somehow?
        result *= (block_1.x[i] == block_2.x[i]);
        result *= (block_1.y[i] == block_2.y[i]);
    }
    result *= (block_1.type == block_2.type);
    result *= (block_1.orientation == block_2.orientation);

    return result;
}

/*
 * Function:    confirm_block
 * --------------------------
 *
 * When a block has successfully reached its final destination, this
 * function is called to move it from current_block to field and to
 * trigger scoring mechanisms.
 *
 * Effects:
 *      field
 *      current_block
 *      block_confirmed
 *
 */

void confirm_block(void)
{
    int i = 0;
    int lockout = 1;

    if ((current_block.type == TILE_NONE) | (block_confirmed))
    {
        return;
    }

    for (i = 0; i < 4; i++)
    {
        field[current_block.x[i]][current_block.y[i]] = current_block.type;
        lockout &= (current_block.y[i] < BUFFER_HEIGHT);
    }
    current_block = protos[0];
    block_confirmed = 1;
    check_for_rows();
    if (lockout)
    {
        is_running = GAMESTATE_LOCKOUT;
    }
}

/*
 * Function:    attempt_rotate
 * ---------------------------
 *
 * Given a block and a direction to rotate, gets the result of
 * attempting to rotate that block in that direction. Does not change
 * the input block. If the block cannot rotate in that direction, will
 * simply return the given block.
 *
 * Once wall-kicks are added, this will also handle wall-kicks.
 *
 * Arguments:
 *      struct block block: The block to attempt to rotate.
 *      int ccw: Whether to rotate counter-clockwise (1) or not (0).
 *
 * Returns:
 *      struct block result: The resulting block, rotated or not.
 *
 */

struct block attempt_rotate(struct block block, int ccw)
{
    struct block temp_block = block;
    struct block result;
    int i;

    // attempt rotate in place
    result = attempt_rotate_sub(temp_block, ccw);
    if (block_is_equal(result, temp_block) == 0) return result;

    // wall-kick: rotate one space to right
    temp_block = block;
    for (i = 0; i < 4; i++)
    {
        temp_block.x[i]++;
    }
    result = attempt_rotate_sub(temp_block, ccw);
    if (block_is_equal(result, temp_block) == 0) return result;

    // wall-kick: rotate one space to left
    temp_block = block;
    for (i = 0; i < 4; i++)
    {
        temp_block.x[i]--;
    }
    result = attempt_rotate_sub(temp_block, ccw);
    if (block_is_equal(result, temp_block) == 0) return result;

    // wall-kick: rotate two spaces to right (if I block)
    if (block.type == TILE_CYAN)
    {
        temp_block = block;
        for (i = 0; i < 4; i++)
        {
            temp_block.x[i] += 2;
        }
        result = attempt_rotate_sub(temp_block, ccw);
        if (block_is_equal(result, temp_block) == 0) return result;
    }

    // wall-kick: rotate two spaces to left (if I block)
    if (block.type == TILE_CYAN)
    {
        temp_block = block;
        for (i = 0; i < 4; i++)
        {
            temp_block.x[i] -= 2;
        }
        result = attempt_rotate_sub(temp_block, ccw);
        if (block_is_equal(result, temp_block) == 0) return result;
    }

    // only allow one floor kick per block
    if (has_floor_kicked) return block;

    // floor-kick: rotate one space up
    temp_block = block;
    for (i = 0; i < 4; i++)
    {
        temp_block.y[i]--;
    }
    result = attempt_rotate_sub(temp_block, ccw);
    if (block_is_equal(result, temp_block) == 0)
    {
        has_floor_kicked = 1;
        return result;
    }

    // floor-kick: rotate two spaces up
    if (block.type == TILE_CYAN)
    {
        temp_block = block;
        for (i = 0; i < 4; i++)
        {
            temp_block.y[i] -= 2;
        }
        result = attempt_rotate_sub(temp_block, ccw);
        if (block_is_equal(result, temp_block) == 0)
        {
            has_floor_kicked = 1;
            return result;
        }
    }

    return block;
}

struct block attempt_rotate_sub(struct block block, int ccw)
{
    // TODO: Assuming clockwise/cw not ccw; no wall-kicks, etc.
    struct block result;

    printf("Block block:\n");
    print_block(block);
    result = block;
    if (ccw)
    {
        switch (block.type)
        {
            case TILE_BLUE:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] + 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 1;
                        break;
                    case ORIENT_EAST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2] - 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2] + 1;
                        result.y[3] = block.y[2];
                        break;
                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] - 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 1;
                        break;
                    case ORIENT_WEST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2] + 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2] - 1;
                        result.y[3] = block.y[2];
                        break;
                    default:
                        break;
                }
                break;

            case TILE_CYAN:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[1];
                        result.y[0] = block.y[1] - 2;
                        result.x[1] = block.x[1];
                        result.y[1] = block.y[1] - 1;
                        result.x[2] = block.x[1];
                        result.y[2] = block.y[1];
                        result.x[3] = block.x[1];
                        result.y[3] = block.y[1] + 1;
                        break;

                    case ORIENT_EAST:
                        result.x[0] = block.x[1] + 2;
                        result.y[0] = block.y[1];
                        result.x[1] = block.x[1] + 1;
                        result.y[1] = block.y[1];
                        result.x[2] = block.x[1];
                        result.y[2] = block.y[1];
                        result.x[3] = block.x[1] - 1;
                        result.y[3] = block.y[1];
                        break;

                    case ORIENT_SOUTH:
                        result.x[0] = block.x[1];
                        result.y[0] = block.y[1] + 2;
                        result.x[1] = block.x[1];
                        result.y[1] = block.y[1] + 1;
                        result.x[2] = block.x[1];
                        result.y[2] = block.y[1];
                        result.x[3] = block.x[1];
                        result.y[3] = block.y[1] - 1;
                        break;

                    case ORIENT_WEST:
                        result.x[0] = block.x[1] - 2;
                        result.y[0] = block.y[1];
                        result.x[1] = block.x[1] - 1;
                        result.y[1] = block.y[1];
                        result.x[2] = block.x[1];
                        result.y[2] = block.y[1];
                        result.x[3] = block.x[1] + 1;
                        result.y[3] = block.y[1];
                        break;

                    default:
                        break;
                }
                break;

            case TILE_GREEN:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2] - 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 1;
                        break;
                    case ORIENT_EAST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] - 1;
                        result.x[3] = block.x[2] - 1;
                        result.y[3] = block.y[2];
                        break;
                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2] + 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 1;
                        break;
                    case ORIENT_WEST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] + 1;
                        result.x[3] = block.x[2] + 1;
                        result.y[3] = block.y[2];
                        break;
                    default:
                        break;
                }
                break;

            case TILE_YELLOW:
                result.x[0] = block.x[3];
                result.y[0] = block.y[3];
                result.x[1] = block.x[0];
                result.y[1] = block.y[0];
                result.x[2] = block.x[1];
                result.y[2] = block.y[1];
                result.x[3] = block.x[2];
                result.y[3] = block.y[2];
                break;

            case TILE_ORANGE:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] - 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 1;
                        break;
                    case ORIENT_EAST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2] + 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2] - 1;
                        result.y[3] = block.y[2];
                        break;
                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] + 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 1;
                        break;
                    case ORIENT_WEST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2] - 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2] + 1;
                        result.y[3] = block.y[2];
                        break;
                    default:
                        break;
                }
                break;

            case TILE_PURPLE:
                result.x[1] = block.x[0];
                result.y[1] = block.y[0];
                result.x[2] = block.x[1];
                result.y[2] = block.y[1];
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[3];
                        result.y[0] = block.y[3] + 1;
                        break;

                    case ORIENT_EAST:
                        result.x[0] = block.x[3] - 1;
                        result.y[0] = block.y[3];
                        break;

                    case ORIENT_SOUTH:
                        result.x[0] = block.x[3];
                        result.y[0] = block.y[3] - 1;
                        break;

                    case ORIENT_WEST:
                        result.x[0] = block.x[3] + 1;
                        result.y[0] = block.y[3];
                        break;

                    default:
                        break;
                }
                break;

            case TILE_RED:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2] - 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 1;
                        break;
                    case ORIENT_EAST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] - 1;
                        result.x[3] = block.x[2] + 1;
                        result.y[3] = block.y[2];
                        break;
                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2] + 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 1;
                        break;
                    case ORIENT_WEST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] + 1;
                        result.x[3] = block.x[2] - 1;
                        result.y[3] = block.y[2];
                        break;
                    default:
                        break;
                }
                break;

            default:
                break;
        }
        result.orientation = (block.orientation - 1) % 4;
    }
    else
    {
        switch (block.type)
        {
            case TILE_BLUE:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] - 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 1;
                        break;
                    case ORIENT_EAST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2] + 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2] - 1;
                        result.y[3] = block.y[2];
                        break;
                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] + 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 1;
                        break;
                    case ORIENT_WEST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2] - 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2] + 1;
                        result.y[3] = block.y[2];
                        break;
                    default:
                        break;
                }
                break;

            case TILE_CYAN:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2];
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2];
                        result.x[2] = block.x[2];
                        result.y[2] = block.y[2] + 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 2;
                        break;

                    case ORIENT_EAST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2];
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2];
                        result.x[2] = block.x[2] - 1;
                        result.y[2] = block.y[2];
                        result.x[3] = block.x[2] - 2;
                        result.y[3] = block.y[2];
                        break;

                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2];
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2];
                        result.x[2] = block.x[2];
                        result.y[2] = block.y[2] - 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 2;
                        break;

                    case ORIENT_WEST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2];
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2];
                        result.x[2] = block.x[2] + 1;
                        result.y[2] = block.y[2];
                        result.x[3] = block.x[2] + 2;
                        result.y[3] = block.y[2];
                        break;

                    default:
                        break;
                }
                break;

            case TILE_GREEN:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2] + 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 1;
                        break;
                    case ORIENT_EAST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] + 1;
                        result.x[3] = block.x[2] + 1;
                        result.y[3] = block.y[2];
                        break;
                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2] - 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 1;
                        break;
                    case ORIENT_WEST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] - 1;
                        result.x[3] = block.x[2] - 1;
                        result.y[3] = block.y[2];
                        break;
                    default:
                        break;
                }
                break;

            case TILE_YELLOW:
                result.x[0] = block.x[1];
                result.y[0] = block.y[1];
                result.x[1] = block.x[2];
                result.y[1] = block.y[2];
                result.x[2] = block.x[3];
                result.y[2] = block.y[3];
                result.x[3] = block.x[0];
                result.y[3] = block.y[0];
                break;

            case TILE_ORANGE:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] + 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 1;
                        break;
                    case ORIENT_EAST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2] - 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2] + 1;
                        result.y[3] = block.y[2];
                        break;
                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] - 1;
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 1;
                        break;
                    case ORIENT_WEST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2] + 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2] - 1;
                        result.y[3] = block.y[2];
                        break;
                    default:
                        break;
                }
                break;

            case TILE_PURPLE:
                result.x[0] = block.x[1];
                result.y[0] = block.y[1];
                result.x[1] = block.x[2];
                result.y[1] = block.y[2];
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[2] = block.x[3];
                        result.y[2] = block.y[3] + 1;
                        break;

                    case ORIENT_EAST:
                        result.x[2] = block.x[3] - 1;
                        result.y[2] = block.y[3];
                        break;

                    case ORIENT_SOUTH:
                        result.x[2] = block.x[3];
                        result.y[2] = block.y[3] - 1;
                        break;

                    case ORIENT_WEST:
                        result.x[2] = block.x[3] + 1;
                        result.y[2] = block.y[3];
                        break;

                    default:
                        break;
                }
                break;

            case TILE_RED:
                switch (block.orientation)
                {
                    case ORIENT_NORTH:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2] + 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] + 1;
                        break;
                    case ORIENT_EAST:
                        result.x[0] = block.x[2] + 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] + 1;
                        result.x[3] = block.x[2] - 1;
                        result.y[3] = block.y[2];
                        break;
                    case ORIENT_SOUTH:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] + 1;
                        result.x[1] = block.x[2] - 1;
                        result.y[1] = block.y[2];
                        result.x[3] = block.x[2];
                        result.y[3] = block.y[2] - 1;
                        break;
                    case ORIENT_WEST:
                        result.x[0] = block.x[2] - 1;
                        result.y[0] = block.y[2] - 1;
                        result.x[1] = block.x[2];
                        result.y[1] = block.y[2] - 1;
                        result.x[3] = block.x[2] + 1;
                        result.y[3] = block.y[2];
                        break;
                    default:
                        break;
                }
                break;

            default:
                break;
        }
        result.orientation = (block.orientation + 1) % 4;
    }
    printf("Block result:\n");
    print_block(result);
    if (can_place_block(result) == 0)
    {
        printf("Cannot rotate block.\n");
        result = block;
        printf("Test\n");
    }
    return result;
}

/*
 * Function:    hard_drop
 * ----------------------
 *
 * Causes the current block to immediately fall to its final
 * destination and confirm. Scores 2 pts for each row.
 *
 * Effects:
 *      current_block
 *
 */

void hard_drop(void)
{
    struct block final;
    final = get_block_final_dest(current_block);
    final.type = current_block.type;
    score_hard_drop(final.y[0] - current_block.y[0]);
    current_block = final;
    confirm_block();
}

/*
 * Function:    can_place_block
 * ---------------------------
 *
 * Determines whether a block can exist in its current location or not.
 * Typically used with tentative moves.
 *
 * Arguments:
 *      struct block block: The block in question.
 *
 * Returns:
 *      int valid: Whether the block can exist in its current location.
 *
 */

int can_place_block(struct block block)
{
    int i = 0;
    int valid = 1;

    for (i = 0; i < 4; i++)
    {
        valid &= (block.x[i] < FIELD_WIDTH);
        valid &= (block.x[i] >= 0);
        valid &= (block.y[i] < FIELD_HEIGHT+BUFFER_HEIGHT);
        valid &= (field[block.x[i]][block.y[i]] == TILE_NONE);
    }

    return valid;
}

/*
 * Function:    fprint_block
 * -------------------------
 *
 * Prints block information to a stream. Useful for debugging.
 *
 * Arguments:
 *      struct block block: The block to print.
 *
 */

void fprint_block(FILE *stream, struct block block)
{
    fprintf(stream,
            "Block position: (%d, %d), (%d, %d), (%d, %d), (%d, %d)\n",
            block.x[0], block.y[0],
            block.x[1], block.y[1],
            block.x[2], block.y[2],
            block.x[3], block.y[3]);
    fprintf(stream,
            "Block color: %d\n",
            block.type);
    fprintf(stream,
            "Block orientation: %d\n",
            block.orientation);
}

/*
 * Function:    print_block
 * ------------------------
 *
 * Prints block information to stdout. Useful for debugging.
 *
 * Arguments:
 *      struct block block: The block to print.
 *
 */

void print_block(struct block block)
{
    fprint_block(stdout, block);
}
