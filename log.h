/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * log.c                                                              *
 **********************************************************************
 *                                                                    *
 * Contains a basic logging structure.                                *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Functions:                                                  *
 *      void debug_log(enum logtype type, char *msg): Logs a message. *
 *                                                                    *
 **********************************************************************/

#ifndef LOG_H
#define LOG_H

enum logtype
{
    LOG_OUTPUT,
    LOG_WARNING,
    LOG_ERROR,
    LOG_CRITICAL
};

void debug_log(enum logtype type, char* msg);

#endif
