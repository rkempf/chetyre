/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * score.h                                                            *
 **********************************************************************
 *                                                                    *
 * Contains scoring algorithms and logic.                             *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      int score: The player's current score.                        *
 *      int level: The player's current level. Used for determining   *
 *                 block speed and points awarded (eventually).       *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_score(void):                                        *
 *          Sets default score and level values.                      *
 *      void load_high_scores(char *filepath):                        *
 *          Loads scores from file.                                   *
 *      void add_high_score(int score):                               *
 *          Adds high score to list.                                  *
 *      void save_high_scores(char *filepath):                        *
 *          Saves scores to file.                                     *
 *      void clean_high_scores(void):                                 *
 *          Frees high score memory.                                  *
 *      void clear_high_scores(void):                                 *
 *          Clears list of high scores.                               *
 *      void score_rows(int num_rows):                                *
 *          Scores a number of rows.                                  *
 *      void score_soft_drop(void):                                   *
 *          Scores when down is pressed.                              *
 *      void score_hard_drop(int num_rows):                           *
 *          Scores a hard drop.                                       *
 *      unsigned int get_move_time(int lvl):                          *
 *          Gets move time for lvl.                                   *
 *                                                                    *
 **********************************************************************/

#ifndef SCORE_H
#define SCORE_H

#define NUM_SCORES 10

struct score
{
    int value;
    struct score *next;
    struct score *prev;
};

extern int current_score;
extern int level;
extern struct score *highscore;

void init_score(void);
void load_high_scores(char *filepath);
void add_high_score(int score);
void save_high_scores(char *filepath);
void clean_high_scores(void);
void clear_high_scores(void);
void score_rows(int num_rows);
void score_soft_drop(void);
void score_hard_drop(int num_rows);
unsigned int get_move_time(int lvl);

#endif
