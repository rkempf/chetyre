/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * log.c                                                              *
 **********************************************************************
 *                                                                    *
 * Contains a basic logging interface.                                *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Functions:                                                  *
 *      void debug_log(enum logtype type, char *msg): Logs a message. *
 *                                                                    *
 **********************************************************************/

#include <stdio.h>
#include "log.h"

/*
 * Function:    debug_log
 * ----------------------
 *
 * Error/message logger. Currently kind of pointless, but in the case
 * that I change how it works (i.e. storing messages?) it is good to
 * abstract errors and other messages. Would be more useful if it
 * allowed printf-style string formatting. Particularly as SDL
 * has its own error message logger.
 *
 * Arguments:
 *      enum logtype type: Where to send the error and how to format it.
 *      char* msg: The error or message.
 *
 */

void debug_log(enum logtype type, char *msg)
{
    switch (type)
    {
        case LOG_OUTPUT:
            printf("%s\n", msg);
            break;
        case LOG_WARNING:
            fprintf(stderr, "WARNING: %s\n", msg);
            break;
        case LOG_ERROR:
            fprintf(stderr, "ERROR: %s\n", msg);
            break;
        case LOG_CRITICAL:
            fprintf(stderr, "CRITICAL: %s\n", msg);
            break;
        default:
            printf("%s\n", msg);
            debug_log(LOG_WARNING, "Invalid logtype!\n");
            break;
    }
}
