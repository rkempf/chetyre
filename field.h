/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * field.h                                                            *
 **********************************************************************
 *                                                                    *
 * Contains code around the play region where blocks fall.            *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      enum tile_type field[][]: The tile grid where blocks fall.    *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_field(void):                                        *
 *          Initializes the field with base values.                   *
 *      void check_for_rows(void):                                    *
 *          Checks if any rows are complete.                          *
 *      void fprint_field(FILE *stream):                              *
 *          Prints field to a stream.                                 *
 *      void print_field(void):                                       *
 *          Prints field to stdout.                                   *
 *                                                                    *
 **********************************************************************/

#ifndef FIELD_H
#define FIELD_H

#define FIELD_HEIGHT 20
#define FIELD_WIDTH 10
#define BUFFER_HEIGHT 2

// Tile type is what color/shape of piece, or none, or ghost. Used
// in many places throughout the game.
enum tile_type
{
    TILE_NONE,
    TILE_BLUE,
    TILE_CYAN,
    TILE_GREEN,
    TILE_ORANGE,
    TILE_PURPLE,
    TILE_RED,
    TILE_YELLOW,
    TILE_GHOST
};

extern enum tile_type field[FIELD_WIDTH][FIELD_HEIGHT+BUFFER_HEIGHT];

void init_field(void);
void check_for_rows(void);
void fprint_field(FILE *stream);
void print_field(void);

#endif
