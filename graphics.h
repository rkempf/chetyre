/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * graphics.h                                                         *
 **********************************************************************
 *                                                                    *
 * Contains graphics rendering and loading code.                      *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Functions:                                                  *
 *      int init_graphics(void):                                      *
 *          Initializes graphics. Call at beginning.                  *
 *      void load_tiles(void):                                        *
 *          Loads tile graphics for init and swapping.                *
 *      void render(void):                                            *
 *          Renders the current game state.                           *
 *      void clean_tiles(void):                                       *
 *          Cleans tile graphics.                                     *
 *      void clean_graphics(void):                                    *
 *          Cleans up. Call at end.                                   *
 *                                                                    *
 **********************************************************************/

#ifndef GRAPHICS_H
#define GRAPHICS_H

int init_graphics(void);
void load_tiles(void);
void render(void);
void clean_tiles(void);
void clean_graphics(void);

#endif
