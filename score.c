/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * score.c                                                            *
 **********************************************************************
 *                                                                    *
 * Contains scoring algorithms and logic.                             *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      int score: The player's current score.                        *
 *      int level: The player's current level. Used for determining   *
 *                 block speed and points awarded (eventually).       *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_score(): Sets default score and level values.       *
 *      void load_high_scores(char *filepath): Loads scores from file.*
 *      void add_high_score(int score): Adds high score to list.      *
 *      void save_high_scores(char *filepath): Saves scores to file.  *
 *      void clean_high_scores(): Frees high score memory.            *
 *      void clear_high_scores(): Clears list of high scores.         *
 *      void score_rows(int num_rows): Scores a number of rows.       *
 *      void score_soft_drop(): Scores when down is pressed.          *
 *      void score_hard_drop(int num_rows): Scores a hard drop.       *
 *      unsigned int get_move_time(int lvl): Gets move time for lvl.  *
 *                                                                    *
 * Private Functions:                                                 *
 *      void set_score(int m): Sets score to m, if possible.          *
 *      struct score *new_score(int score):                           *
 *          Allocates memory and creates a new score pointer.         *
 *                                                                    *
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "score.h"
#include "options.h"

#define MAX_SCORE 999999

struct score *new_score(int score);
void set_score(int m);

// Current score and level are pretty self-explanatory, aren't they?
int current_score;
int level;
// Pointers to the beginning and end of the linked list
// containing the list of high scores, respectively.
struct score *highscore;
struct score *endptr;
// rows_scored stores how many rows have been scored so far for
// the purpose of determining level.
int rows_scored;
// streak is how many sequential scores there have been.
int streak;

/*
 * Function:    init_score
 * -----------------------
 *
 * Initializes the scoring system, with no points and at level 1.
 *
 * Effects:
 *      current_score
 *      level
 *
 */

void init_score(void)
{
    set_score(0);
    level = selected_options[OPTION_STARTINGLEVEL];
}

/*
 * Function:    load_high_scores
 * -----------------------------
 *
 * Loads high scores from a given filepath.
 *
 * Arguments:
 *      char* filepath: The filepath where scores are kept.
 *          (current iteration: highscores.txt)
 *
 * Effects:
 *      highscore
 *      endptr
 *
 */

void load_high_scores(char *filepath)
{
    int score = 0;
    FILE *scores_list;

    scores_list = fopen(filepath, "r");
    if (scores_list == 0)
    {
        fprintf(stderr, "Cannot open file %s!\n", filepath);
        return;
    }

    while (fscanf(scores_list, "%d\n", &score) != EOF)
    {
        add_high_score(score);
    }

    if (highscore == NULL)
    {
        add_high_score(0);
    }

    fclose(scores_list);
}

/*
 * Function:    save_high_scores
 * -----------------------------
 *
 * Saves high scores to a given filepath.
 *
 * Arguments:
 *      char* filepath: The filepath where scores are kept.
 *          (current iteration: highscores.txt)
 *
 */

void save_high_scores(char *filepath)
{
    FILE *scores_list;
    struct score *cursor = highscore;

    scores_list = fopen(filepath, "w");
    if (scores_list == 0)
    {
        fprintf(stderr, "Cannot open file %s!\n", filepath);
        return;
    }

    while (cursor)
    {
        if(cursor->value > 0)
        {
            fprintf(scores_list, "%d\n", cursor->value);
        }
        cursor = cursor->next;
    }

    fclose(scores_list);
}

/*
 * Function:    clean_high_scores
 * ------------------------------
 *
 * Frees memory allocated to the high score list.
 *
 * Effects:
 *      highscore
 *      endptr
 *
 */

void clean_high_scores(void)
{
    struct score *cursor = endptr;

    while (cursor->prev)
    {
        cursor = cursor->prev;
        free(cursor->next);
        cursor->next = NULL;
    }

    free(cursor);

    highscore = NULL;
    endptr = NULL;
}

/*
 * Function:    clear_high_scores
 * ------------------------------
 *
 * Deletes all high scores.
 *
 * Effects:
 *      highscore
 *      endptr
 *
 */

void clear_high_scores(void)
{
    struct score *cursor = endptr;

    while (cursor->prev)
    {
        cursor = cursor->prev;
        free(cursor->next);
        cursor->next = NULL;
    }

    free(cursor);

    highscore = NULL;
    endptr = NULL;

    add_high_score(0);
}

/*
 * Function:    add_high_score
 * ---------------------------
 *
 * Given a numeric score, adds it to the list of high scores.
 *
 * Arguments:
 *      int score: The score to add.
 *
 * Effects:
 *      highscore
 *      endptr
 *
 */

void add_high_score(int score)
{
    struct score *cursor = highscore;
    if (!cursor)
    {
        highscore = new_score(score);
        endptr = highscore;
        return;
    }

    if (score > highscore->value)
    {
        highscore->prev = new_score(score);
        highscore->prev->next = highscore;
        highscore = highscore->prev;
        return;
    }

    while (cursor)
    {
        if (cursor->value >= score)
        {
            if (!(cursor->next))
            {
                cursor->next = new_score(score);
                cursor->next->prev = cursor;
                endptr = cursor->next;
                break;
            }
            if (cursor->next->value <= score)
            {
                cursor->next->prev = new_score(score);
                cursor->next->prev->next = cursor->next;
                cursor->next->prev->prev = cursor;
                cursor->next = cursor->next->prev;
                break;
            }
        }

        cursor = cursor->next;
    }
}

/*
 * Function:    new_score
 * ----------------------
 *
 * Given values of the elements, allocates memory for a new
 * score pointer and returns it.
 *
 * Arguments:
 *      int score: The numeric score, for the element value.
 *
 * Returns:
 *      (struct score)* return_score: A pointer to the allocated memory.
 *
 */

struct score *new_score(int score)
{
    struct score *return_score = malloc(sizeof(struct score));
    if (return_score == 0) return NULL;

    return_score->value = score;
    return_score->next = NULL;
    return_score->prev = NULL;
    return return_score;
}

/*
 * Function:    score_rows
 * -----------------------
 *
 * Scores a given number of rows. More points for more rows; more
 * points for higher level.
 *
 * Arguments:
 *      int num_rows: The number of rows to score.
 *
 * Effects:
 *      current_score
 *      streak
 *
 */

void score_rows(int num_rows)
{
    int additional = 0;

    switch (num_rows)
    {
        case 1:
            additional = (int) (100 * level * pow(1.5, streak));
            set_score(current_score + additional);
            break;

        case 2:
            additional = (int) (300 * level * pow(1.5, streak));
            set_score(current_score + additional);
            break;

        case 3:
            additional = (int) (500 * level * pow(1.5, streak));
            set_score(current_score + additional);
            break;

        case 4:
            additional = (int) (800 * level * pow(1.5, streak));
            set_score(current_score + additional);
            break;

        default:
            break;
    }
    streak = (num_rows > 0 ? streak+1 : 0);
    printf("Level %d; rows %d; additional %d\n", level, num_rows, additional);
    rows_scored += num_rows;
    if (rows_scored >= level * 10)
    {
        level++;
        printf("Setting level to: %d\n", level);
    }
}

/*
 * Function:    score_soft_drop
 * ----------------------------
 *
 * Called every time the "down" button is pressed during the game
 * and moves the piece downwards. Scores 1 point (per move down).
 *
 * Effects:
 *      current_score
 *
 */

void score_soft_drop(void)
{
    set_score(current_score + 1);
}

/*
 * Function:    score_hard_drop
 * ----------------------------
 *
 * Called when the confirm button is pressed and causes a hard drop,
 * in order to score twice that many points in the game. Hard drop
 * is currently unimplemented.
 *
 * Arguments:
 *      int num_rows: The number of rows the block drops.
 *
 * Effects:
 *      current_score
 *
 */

void score_hard_drop(int num_rows)
{
    set_score(current_score + (2 * num_rows));
}

/*
 * Function:    set_score
 * ----------------------
 *
 * Setter function for score, so that score doesn't go above the max.
 * Negative score is currently disabled, but may be reenabled in
 * the future.
 *
 * Arguments:
 *      int m: What to set the score to, if possible.
 *
 * Effects:
 *      current_score
 *
 */

void set_score(int m)
{
    current_score = m;
    if (current_score > MAX_SCORE) current_score = MAX_SCORE;
    if (current_score < 0) current_score = 0;
}

/*
 * Function:    get_move_time
 * --------------------------
 *
 * Calculates the move time corresponding to a given level.
 *
 * Arguments:
 *      int lvl: The level to get the move time for.
 *
 * Returns:
 *      unsigned int move_time: The calculated move time.
 *
 */
unsigned int get_move_time(int lvl)
{
    float tmp = (0.8 - ((lvl - 1) * 0.007));
    tmp = powf(tmp, lvl - 1) * 1000;
    return (unsigned int)tmp;
}
