/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2020 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * options.h                                                          *
 **********************************************************************
 *                                                                    *
 * Governs options menu logic.                                        *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      int[13] selected_options: Current options choices.            *
 *      int[13] min_options: Minimum value for each option.           *
 *      int[13] max_options: Maximum value for each option.           *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_options(void):                                      *
 *          Initializes options with default values.                  *
 *      void setup_options_menu(void):                                *
 *          Sets up initial options menu state.                       *
 *      void change_value_current_option(int direction):              *
 *          Changes the value of the current option.                  *
 *      void change_current_option(int direction):                    *
 *          Changes the currently selected option.                    *
 *      void save_options(void):                                      *
 *          Saves options to a file.                                  *
 *      void load_options(void):                                      *
 *          Loads options from a file.                                *
 *                                                                    *
 **********************************************************************/

#ifndef OPTIONS_H
#define OPTIONS_H

enum option
{
    OPTION_BLOCKCHOICE,
    OPTION_GRID,
    OPTION_GHOSTS,
    OPTION_NEXTBLOCK,
    OPTION_BLOCKCOLORS,
    OPTION_STARTINGLEVEL,
    OPTION_SIZE,
    OPTION_MUSIC,
    OPTION_MUSICVOLUME,
    OPTION_LANGUAGE,
    OPTION_CLEARHIGHSCORES = OPTION_LANGUAGE + 2,
    OPTION_BACKTOMAINMENU,
    OPTION_MIN = OPTION_BLOCKCHOICE,
    OPTION_MAX = OPTION_BACKTOMAINMENU
};

enum option_values
{
    BLOCKCHOICE_GRABBAG = 0,
    BLOCKCHOICE_RANDOM = 1,
    BLOCKCHOICE_SADISTIC = 2,
    GRID_ON = 0,
    GRID_OFF = 1,
    GHOSTS_ON = 0,
    GHOSTS_OFF = 1,
    NEXTBLOCK_ON = 0,
    NEXTBLOCK_OFF = 1,
    BLOCKCOLORS_ON = 0,
    BLOCKCOLORS_OFF = 1,
    SIZE_384 = 0,
    SIZE_768 = 1,
    MUSIC_KOROBEINIKI = 0,
    MUSIC_KATYUSHA = 1,
    MUSIC_NONE = 2,
    LANGUAGE_ENGLISH = 0,
    LANGUAGE_RUSSIAN = 1
};

extern int selected_options[13];
extern int min_options[13]; // min val of each option
extern int max_options[13]; // max val of each option

void init_options(void);
void setup_options_menu(void);
void change_value_current_option(int direction);
void change_current_option(int direction);
void save_options(void);
void load_options(void);

#endif
