/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * controls.c                                                         *
 **********************************************************************
 *                                                                    *
 * Contains code dealing with setting keyboard controls.              *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      enum game_event events_from_keys[NUM_KEYS]:                   *
 *          Lookup from SDL_Scancodes to Game Events.                 *
 *      SDL_Scancode keys_from_events[GAMEEVENT_MAX+1]                *
 *          Lookup from Game Events to SDL_Scancodes.                 *
 *      char *strings_from_keys[NUM_KEYS]                             *
 *          Lookup from SDL_Scancodes to printable strings.           *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_keys(void):                                         *
 *          Sets up key-binding environment and initial keybinds.     *
 *      void set_key_command(SDL_Scancode sc, enum game_event ge):    *
 *          Links a key to an event and vice-versa.                   *
 *      void load_key_config(void):                                   *
 *          Loads key config information from a file.                 *
 *      void save_key_config(void):                                   *
 *          Saves key config information to a file.                   *
 *      void change_current_control(int direction):                   *
 *          Changes the selected control in a given direction.        *
 *      void set_current_option_waiting(void):                        *
 *          Sets the current control to await a new key value.        *
 *      void init_controls_menu(void):                                *
 *          Initializes the controls menu.                            *
 *      void set_key_defaults(void):                                  *
 *          Sets or resets keybinding defaults.                       *
 *                                                                    *
 **********************************************************************/

#include <wchar.h>
#include "controls.h"
#include "events.h"
#include "menu.h"

enum game_event events_from_keys[NUM_KEYS];
SDL_Scancode keys_from_events[GAMEEVENT_MAX+1];
wchar_t *strings_from_keys[NUM_KEYS] =
{
    L"Unknown",
    L"",
    L"",
    L"",
    L"A",
    L"B",
    L"C",
    L"D",
    L"E",
    L"F",
    L"G",
    L"H",
    L"I",
    L"J",
    L"K",
    L"L",
    L"M",
    L"N",
    L"O",
    L"P",
    L"Q",
    L"R",
    L"S",
    L"T",
    L"U",
    L"V",
    L"W",
    L"X",
    L"Y",
    L"Z",
    L"1",
    L"2",
    L"3",
    L"4",
    L"5",
    L"6",
    L"7",
    L"8",
    L"9",
    L"0",
    L"Return",
    L"Escape",
    L"Backspace",
    L"Tab",
    L"Space",
    L"Minus",
    L"Equals",
    L"Left Bracket",
    L"Right Bracket",
    L"Backslash",
    L"Non-US Hash",
    L"Semicolon",
    L"Single Quote",
    L"Backtick",
    L"Comma",
    L"Period",
    L"Slash",
    L"Caps Lock",
    L"F1",
    L"F2",
    L"F3",
    L"F4",
    L"F5",
    L"F6",
    L"F7",
    L"F8",
    L"F9",
    L"F10",
    L"F11",
    L"F12",
    L"Print Screen",
    L"Scroll Lock",
    L"Pause",
    L"Insert",
    L"Home",
    L"Page Up",
    L"Delete",
    L"End",
    L"Page Down",
    L"Right",
    L"Left",
    L"Down",
    L"Up",
    L"Numlock",
    L"Keypad Slash",
    L"Keypad Multiply",
    L"Keypad Minus",
    L"Keypad Plus",
    L"Keypad Enter",
    L"Keypad 1",
    L"Keypad 2",
    L"Keypad 3",
    L"Keypad 4",
    L"Keypad 5",
    L"Keypad 6",
    L"Keypad 7",
    L"Keypad 8",
    L"Keypad 9",
    L"Keypad 0",
    L"Keypad Period",
    L"Non-US Backslash",
    L"Application",
    L"Power",
    L"Keypad Equals",
    L"F13",
    L"F14",
    L"F15",
    L"F16",
    L"F17",
    L"F18",
    L"F19",
    L"F20",
    L"F21",
    L"F22",
    L"F23",
    L"F24",
    L"Execute",
    L"Help",
    L"Menu",
    L"Select",
    L"Stop",
    L"Again",
    L"Undo",
    L"Cut",
    L"Copy",
    L"Paste",
    L"Find",
    L"Mute",
    L"Volume Up",
    L"Volume Down",
    L"",
    L"",
    L"",
    L"Keypad Comma",
    L"Keypad Equals AS400",
    L"International 1",
    L"International 2",
    L"International 3",
    L"International 4",
    L"International 5",
    L"International 6",
    L"International 7",
    L"International 8",
    L"International 9",
    L"Lang 1",
    L"Lang 2",
    L"Lang 3",
    L"Lang 4",
    L"Lang 5",
    L"Lang 6",
    L"Lang 7",
    L"Lang 8",
    L"Lang 9",
    L"AltErase",
    L"SysReq",
    L"Cancel",
    L"Clear",
    L"Prior",
    L"Return",
    L"Separator",
    L"Out",
    L"Oper",
    L"Clear Again",
    L"CrSel",
    L"ExSel",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"Keypad 00",
    L"Keypad 000",
    L"Thousands Separator",
    L"Decimal Separator",
    L"Currency Unit",
    L"Currency SubUnit",
    L"Keypad Left Paren",
    L"Keypad Right Paren",
    L"Keypad Left Brace",
    L"Keypad Right Brace",
    L"Keypad Tab",
    L"Keypad Backspace",
    L"Keypad A",
    L"Keypad B",
    L"Keypad C",
    L"Keypad D",
    L"Keypad E",
    L"Keypad F",
    L"Keypad XOR",
    L"Keypad Power",
    L"Keypad Percent",
    L"Keypad Less",
    L"Keypad Greater",
    L"Keypad Ampersand",
    L"Keypad Double Ampersand",
    L"Keypad Pipe",
    L"Keypad Double Pipe",
    L"Keypad Colon",
    L"Keypad Hash",
    L"Keypad Space",
    L"Keypad At",
    L"Keypad Exclam",
    L"Keypad MemStore",
    L"Keypad MemRecall",
    L"Keypad MemClear",
    L"Keypad MemAdd",
    L"Keypad MemSubtract",
    L"Keypad MemMultiply",
    L"Keypad MemDivide",
    L"Keypad Plus Minus",
    L"Keypad Clear",
    L"Keypad Clear Entry",
    L"Keypad Binary",
    L"Keypad Octal",
    L"Keypad Decimal",
    L"Keypad Hexadecimal",
    L"",
    L"",
    L"Left Ctrl",
    L"Left Shift",
    L"Left Alt",
    L"Left GUI",
    L"Right Ctrl",
    L"Right Shift",
    L"Right Alt",
    L"Right GUI",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"",
    L"Mode Switch",
    L"Audio Next",
    L"Audio Prev",
    L"Audio Stop",
    L"Audio Play",
    L"Audio Mute",
    L"Media Select",
    L"WWW",
    L"Mail",
    L"Calculator",
    L"Computer",
    L"AC Search",
    L"AC Home",
    L"AC Back",
    L"AC Forward",
    L"AC Stop",
    L"AC Refresh",
    L"AC Bookmarks",
    L"Brightness Down",
    L"Brightness Up",
    L"Display Switch",
    L"Keyboard Illum Toggle",
    L"Keyboard Illum Down",
    L"Keyboard Illum Up",
    L"Eject",
    L"Sleep",
    L"App 1",
    L"App 2"
};
int strlen_from_keys[NUM_KEYS] =
{
    7,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    6,
    6,
    9,
    3,
    5,
    5,
    6,
    12,
    13,
    9,
    11,
    9,
    12,
    8,
    5,
    6,
    5,
    9,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    3,
    3,
    3,
    12,
    11,
    5,
    6,
    4,
    7,
    6,
    3,
    9,
    5,
    4,
    4,
    2,
    7,
    12,
    15,
    12,
    11,
    12,
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    13,
    16,
    11,
    5,
    13,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    3,
    7,
    4,
    4,
    6,
    4,
    5,
    4,
    3,
    4,
    5,
    4,
    4,
    9,
    11,
    0,
    0,
    0,
    12,
    19,
    15,
    15,
    15,
    15,
    15,
    15,
    15,
    15,
    15,
    6,
    6,
    6,
    6,
    6,
    6,
    6,
    6,
    6,
    8,
    6,
    6,
    5,
    5,
    6,
    9,
    3,
    4,
    11,
    5,
    5,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    9,
    10,
    19,
    17,
    13,
    16,
    17,
    18,
    17,
    18,
    10,
    16,
    8,
    8,
    8,
    8,
    8,
    8,
    10,
    12,
    14,
    11,
    14,
    16,
    23,
    11,
    18,
    12,
    11,
    12,
    9,
    13,
    15,
    16,
    15,
    13,
    18,
    18,
    16,
    17,
    12,
    18,
    13,
    12,
    14,
    18,
    0,
    0,
    9,
    10,
    8,
    8,
    10,
    11,
    9,
    9,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    11,
    10,
    10,
    10,
    10,
    10,
    12,
    3,
    4,
    10,
    8,
    9,
    7,
    7,
    10,
    7,
    10,
    12,
    15,
    13,
    14,
    21,
    19,
    17,
    5,
    5,
    5,
    5
};
enum game_event events_from_controls[CONTROLS_MAX+1] =
{
    GAMEEVENT_QUIT,
    GAMEEVENT_LEFT,
    GAMEEVENT_RIGHT,
    GAMEEVENT_UP,
    GAMEEVENT_DOWN,
    GAMEEVENT_CCW,
    GAMEEVENT_CANCEL,
    GAMEEVENT_CONFIRM,
    GAMEEVENT_NONE,
    GAMEEVENT_NONE,
    GAMEEVENT_NONE,
    GAMEEVENT_NONE,
    GAMEEVENT_NONE
};
enum controls controls_from_events[GAMEEVENT_MAX+1] =
{
    CONTROLS_NONE,
    CONTROLS_QUIT,
    CONTROLS_LEFT,
    CONTROLS_RIGHT,
    CONTROLS_UP,
    CONTROLS_DOWN,
    CONTROLS_CCW,
    CONTROLS_CANCEL,
    CONTROLS_CONFIRM
};
int waiting;

/*
 * Function:    set_key_command
 * ----------------------------
 *
 * Sets lookups for a given key to a given game event.
 *
 * Arguments:
 *      SDL_Scancode sc: Key scancode lookup.
 *      enum game_event ge: Game event lookup.
 *
 * Effects:
 *      events_from_keys
 *      keys_from_events
 *
 */

void set_key_command(SDL_Scancode sc, enum game_event ge)
{
    SDL_Scancode old_sc;
    enum game_event old_ge;

    if (ge == GAMEEVENT_NONE) return;
    if (sc == 0) return;

    old_ge = events_from_keys[sc];
    old_sc = keys_from_events[ge];
    if ((sc == old_sc) && (ge == old_ge)) return;

    if (old_ge != GAMEEVENT_NONE)
    {
        waiting = controls_from_events[old_ge];
        controls_menu_selected = waiting;
    }
    else
    {
        waiting = -1;
    }

    keys_from_events[old_ge] = 0;
    events_from_keys[old_sc] = GAMEEVENT_NONE;
    events_from_keys[sc] = ge;
    keys_from_events[ge] = sc;
}

/*
 * Function:    init_keys
 * ----------------------
 *
 * Initializes event lookup for keybindings.
 *
 * Effects:
 *      events_from_keys
 *      keys_from_events
 *
 */

void init_keys()
{
    int i;

    for (i = 0; i < NUM_KEYS; i++)
    {
        events_from_keys[i] = GAMEEVENT_NONE;
    }
    for (i = GAMEEVENT_MIN; i <= GAMEEVENT_MAX; i++)
    {
        keys_from_events[i] = 0;
    }

    set_key_defaults();

    load_key_config();

    waiting = -1;
}

/*
 * Function:    load_key_config
 * ----------------------------
 *
 * Loads key configuration from a file.
 *
 * Effects:
 *      events_from_keys
 *      keys_from_events
 *
 */

void load_key_config()
{
    int sc;
    int ge;
    FILE *key_config;

    key_config = fopen("keys.txt", "r");

    if (key_config == 0)
    {
        printf("Cannot load key config from file.\n");
        return;
    }

    while (fscanf(key_config, "%d %d\n", &sc, &ge) != EOF)
    {
        if ((sc < 0) || (sc >= NUM_KEYS) ||
            (ge < 0) || (ge > GAMEEVENT_MAX)) continue;

        set_key_command(sc, ge);
    }

    fclose(key_config);
}

/*
 * Function:    save_key_config
 * ----------------------------
 *
 * Saves key configuration to a file.
 *
 * Effects:
 *      events_from_keys
 *      keys_from_events
 *      keys.txt
 *
 */

void save_key_config()
{
    FILE *key_config;
    int i;

    key_config = fopen("keys.txt", "w");

    if (key_config == 0)
    {
        printf("Cannot save key config to file.\n");
        return;
    }

    for (i = 0; i < NUM_KEYS; i++)
    {
        if (events_from_keys[i] == GAMEEVENT_NONE) continue;
        fprintf(key_config, "%d %d\n", i, events_from_keys[i]);
    }

    fclose(key_config);
}

/*
 * Function:    change_current_control
 * -----------------------------------
 *
 * Changes which control is currently selected by the cursor.
 *
 * Arguments:
 *      int direction: Positive -> down, negative -> up.
 *
 * Effects:
 *      controls_menu_selected
 *
 */

void change_current_control(int direction)
{
    int dir;
    if (direction > 0)
    {
        dir = 1;
    }
    else if (direction < 0)
    {
        dir = -1;
    }
    else
    {
        return;
    }

    // find next (less squirrelly, but more hardcoded here)
    if (controls_menu_selected + dir > CONTROLS_MAX)
    {
        controls_menu_selected = CONTROLS_MAX;
    }
    else if (controls_menu_selected + dir < CONTROLS_MIN)
    {
        controls_menu_selected = CONTROLS_MIN;
    }
    else if ((controls_menu_selected == CONTROLS_CONFIRM) && (dir == 1))
    {
        controls_menu_selected = CONTROLS_RESET;
    }
    else if ((controls_menu_selected == CONTROLS_RESET) && (dir == -1))
    {
        controls_menu_selected = CONTROLS_CONFIRM;
    }
    else
    {
        controls_menu_selected += dir;
    }

    printf("Direction: %d; dir: %d\n", direction, dir);
    printf("Setting current option to: %d\n", controls_menu_selected);
}

/*
 * Function:    set_current_option_waiting
 * ---------------------------------------
 *
 * Sets program to await a new key value for the currently
 * selected control.
 *
 * Effects:
 *      waiting
 *
 */

void set_current_option_waiting()
{
    waiting = controls_menu_selected;
}

/*
 * Function:    init_controls_menu
 * -------------------------------
 *
 * Sets up controls menu with initial values.
 *
 * Effects:
 *      controls_menu_selected
 *      waiting
 *
 */

void init_controls_menu()
{
    controls_menu_selected = 0;
    waiting = -1;
}

/*
 * Function:    set_key_defaults
 * -----------------------------
 *
 * Sets or resets keybindings to default values.
 *
 */

void set_key_defaults()
{
    set_key_command(SDL_SCANCODE_Q, GAMEEVENT_QUIT);
    //set_key_command(SDL_SCANCODE_A, GAMEEVENT_LEFT);
    set_key_command(SDL_SCANCODE_LEFT, GAMEEVENT_LEFT);
    //set_key_command(SDL_SCANCODE_KP_4, GAMEEVENT_LEFT);
    //set_key_command(SDL_SCANCODE_D, GAMEEVENT_RIGHT);
    set_key_command(SDL_SCANCODE_RIGHT, GAMEEVENT_RIGHT);
    //set_key_command(SDL_SCANCODE_KP_6, GAMEEVENT_RIGHT);
    //set_key_command(SDL_SCANCODE_W, GAMEEVENT_UP);
    set_key_command(SDL_SCANCODE_UP, GAMEEVENT_UP);
    //set_key_command(SDL_SCANCODE_KP_8, GAMEEVENT_UP);
    //set_key_command(SDL_SCANCODE_S, GAMEEVENT_DOWN);
    set_key_command(SDL_SCANCODE_DOWN, GAMEEVENT_DOWN);
    //set_key_command(SDL_SCANCODE_KP_2, GAMEEVENT_DOWN);
    set_key_command(SDL_SCANCODE_LCTRL, GAMEEVENT_CCW);
    //set_key_command(SDL_SCANCODE_X, GAMEEVENT_CANCEL);
    set_key_command(SDL_SCANCODE_ESCAPE, GAMEEVENT_CANCEL);
    //set_key_command(SDL_SCANCODE_C, GAMEEVENT_CONFIRM);
    set_key_command(SDL_SCANCODE_RETURN, GAMEEVENT_CONFIRM);
    //set_key_command(SDL_SCANCODE_KP_ENTER, GAMEEVENT_CONFIRM);
    //set_key_command(SDL_SCANCODE_SPACE, GAMEEVENT_CONFIRM);
}
