/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * music.c                                                            *
 **********************************************************************
 *                                                                    *
 * Governs music loading and playing.                                 *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      enum song active_song: Currently selected song.               *
 *                                                                    *
 * Public Functions:                                                  *
 *      int init_music(void):                                         *
 *          Initializes music handler.                                *
 *      void clean_audio(void):                                       *
 *          Cleans up music handler at end of game.                   *
 *      void play_music(void):                                        *
 *          Plays selected music.                                     *
 *      void pause_music(void):                                       *
 *          Pauses selected music.                                    *
 *      void stop_music(int fade):                                    *
 *          Stops playing music. If fade is true, fades out first.    *
 *      void switch_music(enum song new_song):                        *
 *          Switches currently active music to play to new_song.      *
 *      void set_music_volume(int level):                             *
 *          Sets music volume level.                                  *
 *                                                                    *
 * Private Functions:                                                 *
 *      enum audio_state load_media(void):                            *
 *          Loads all music files into the game.                      *
 *                                                                    *
 **********************************************************************/

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include "music.h"
#include "options.h"

Mix_Music *korobeiniki = NULL;
Mix_Music *katyusha = NULL;
enum audio_state audio_status = AUD_NOT_LOADED;
enum song active_song = SONG_NONE;

enum audio_state load_media(void);

/*
 * Function:    init_music
 * -----------------------
 *
 * Initializes music handlers.
 *
 * Effects:
 *      audio_status
 *
 * Returns:
 *      int audio_status: Success or failure. 0 or less is success.
 *
 */

int init_music(void)
{
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
    {
        printf("SDL Mixer could not initialize! Err: %s\n", Mix_GetError());
        audio_status = AUD_INIT_FAIL;
    }
    else
    {
        audio_status = AUD_SUCCESS;
        load_media();
        set_music_volume(selected_options[OPTION_MUSICVOLUME]);
        play_music();
    }

    return audio_status;
}

/*
 * Function:    load_media
 * -----------------------
 *
 * Loads all music files used by the game. Flags if any fail in
 * audio_status.
 *
 * Effects:
 *      audio_status
 *      korobeiniki
 *      katyusha
 *      active_song
 *
 * Returns:
 *      enum audio_state audio_status: Success or failure.
 *
 */

enum audio_state load_media(void)
{
    korobeiniki = Mix_LoadMUS("assets/korobeiniki.wav");
    if (korobeiniki == NULL)
    {
        printf("Failed to load korobeiniki.wav! Err: %s\n", Mix_GetError());
        audio_status = audio_status & AUD_LOAD_FAIL_KOR;
    }

    katyusha = Mix_LoadMUS("assets/katyusha.wav");
    if (katyusha == NULL)
    {
        printf("Failed to load katyusha.wav! Err: %s\n", Mix_GetError());
        audio_status = audio_status & AUD_LOAD_FAIL_KAT;
    }

    if ((selected_options[OPTION_MUSIC] == 0) &
       !(audio_status & AUD_LOAD_FAIL_KOR))
    {
        active_song = SONG_KOROBEINIKI;
    }
    else if ((selected_options[OPTION_MUSIC] == 1) &
       !(audio_status & AUD_LOAD_FAIL_KAT))
    {
        active_song = SONG_KATYUSHA;
    }
    else
    {
        active_song = SONG_NONE;
    }

    return audio_status;
}

/*
 * Function:    clean_audio
 * ------------------------
 *
 * Cleans up audio references after the game is complete.
 *
 * Effects:
 *      audio_status
 *      korobeiniki
 *      katyusha
 *
 */

void clean_audio(void)
{
    stop_music(0);

    Mix_FreeMusic(korobeiniki);
    korobeiniki = NULL;

    Mix_FreeMusic(katyusha);
    katyusha = NULL;

    Mix_Quit();
}

/*
 * Function:    play_music
 * -----------------------
 *
 * Plays currently selected music.
 *
 */

void play_music(void)
{
    // if there is no music playing:
    if (Mix_PlayingMusic() == 0)
    {
        switch (active_song)
        {
            case SONG_NONE:
                break;
            case SONG_KOROBEINIKI:
                if ((audio_status & AUD_LOAD_FAIL_KOR) != AUD_LOAD_FAIL_KOR)
                {
                    if (Mix_PlayMusic(korobeiniki, -1) == -1)
                    {
                        printf("Mix_PlayMusic: %s\n", Mix_GetError());
                    }
                }
                break;
            case SONG_KATYUSHA:
                if ((audio_status & AUD_LOAD_FAIL_KAT) != AUD_LOAD_FAIL_KAT)
                {
                    if (Mix_PlayMusic(katyusha, -1) == -1)
                    {
                        printf("Mix_PlayMusic: %s\n", Mix_GetError());
                    }
                }
                break;
            default:
                break;
        }
    }
    else if (Mix_PausedMusic() == 1)
    {
        Mix_ResumeMusic();
    }
}

/*
 * Function:    pause_music
 * ------------------------
 *
 * Pauses currently playing music.
 *
 */


void pause_music(void)
{
    if ((Mix_PlayingMusic() == 1) && (Mix_PausedMusic() == 0))
    {
        Mix_PauseMusic();
    }
}

/*
 * Function:    stop_music
 * -----------------------
 *
 * Stops currently playing music.
 *
 * Arguments:
 *      int fade: Boolean for whether to fadeout (1) or stop outright (0).
 *
 */

void stop_music(int fade)
{
    if (Mix_PlayingMusic() == 1)
    {
        if ((Mix_PausedMusic() == 0) && (fade == 1))
        {
            Mix_FadeOutMusic(1000);
        }
        else
        {
            Mix_HaltMusic();
        }
    }
}

/*
 * Function:    switch_music
 * -----------------------
 *
 * Switches currently active song.
 *
 * Arguments:
 *      enum song new_song: song to switch to.
 *
 * Effects:
 *      active_song
 *
 */

void switch_music(enum song new_song)
{
    if (Mix_PlayingMusic() == 0)
    {
        active_song = new_song;
        return;
    }

    if (Mix_PausedMusic() == 1)
    {
        active_song = new_song;
        return;
    }

    if (active_song == new_song)
    {
        return;
    }

    if (active_song != SONG_NONE)
    {
        Mix_FadeOutMusic(0);
    }
    
    switch (new_song)
    {
        case SONG_KOROBEINIKI:
            if ((audio_status & AUD_LOAD_FAIL_KOR) != AUD_LOAD_FAIL_KOR)
            {
                if (Mix_PlayMusic(korobeiniki, -1) == -1)
                {
                    printf("Mix_PlayMusic: %s\n", Mix_GetError());
                }
            }
            active_song = new_song;
            break;

        case SONG_KATYUSHA:
            if ((audio_status & AUD_LOAD_FAIL_KAT) != AUD_LOAD_FAIL_KAT)
            {
                if (Mix_PlayMusic(katyusha, -1) == -1)
                {
                    printf("Mix_PlayMusic: %s\n", Mix_GetError());
                }
            }
            active_song = new_song;
            break;

        case SONG_NONE:
            stop_music(0);
            active_song = new_song;
            break;

        default:
            active_song = new_song;
    }
}

void set_music_volume(int level)
{
    int vol;
    vol = level * MIX_MAX_VOLUME / 9;
    Mix_VolumeMusic(vol);
}
