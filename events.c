/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * events.c                                                           *
 **********************************************************************
 *                                                                    *
 * Contains logic to handle events and key presses and delegate them  *
 * to the appropriate locations.                                      *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Functions:                                                  *
 *      void handle_events(void):                                     *
 *          Event handler. Polls for SDL_Events, and if one is        *
 *          provided, delegates or handles it.                        *
 *                                                                    *
 * Private Functions:                                                 *
 *      void handle_keyboard_input(SDL_Event event): Handles keyboard *
 *                                                   input.           *
 *      void call_game_event(enum GameEvent event): Used when a game  *
 *                                                  event is called.  *
 *                                                  Handles it.       *
 *                                                                    *
 **********************************************************************/

#include <SDL2/SDL.h>
#include "events.h"
#include "game.h"
#include "block.h"
#include "log.h"
#include "score.h"
#include "menu.h"
#include "options.h"
#include "controls.h"
#include "music.h"

void handle_keyboard_input(SDL_Event event);
void call_game_event(enum game_event event);

// TODO: handle key holding

/*
 * Function:    handle_events
 * --------------------------
 *
 * Processes the current SDL_Event and calls the appropriate next step.
 *
 */

void handle_events(void)
{
    SDL_Event event;
    SDL_PollEvent(&event);

    switch (event.type)
    {
        case SDL_QUIT:
            call_game_event(GAMEEVENT_QUIT);
            break;

        case SDL_KEYDOWN:
            handle_keyboard_input(event);
            break;

        default:
            break;
    }
}

/*
 * Function:    handle_keyboard_input
 * ----------------------------------
 *
 * Given an SDL_Event (specifically, an SDL_KeyEvent), processes the
 * keystroke and calls the appropriate game event.
 *
 * Arguments:
 *      SDL_Event event: the relevant SDL_KeyEvent with keyboard input.
 *
 */

void handle_keyboard_input(SDL_Event event)
{
    SDL_Keycode key = event.key.keysym.sym;
    SDL_Scancode sc = SDL_GetScancodeFromKey(key);

    if ((current_menu == MENU_CONTROLS) &&
        (controls_menu_selected < CONTROLS_RESET) &&
        (waiting >= 0) &&
        (waiting == controls_menu_selected))
    {
        set_key_command(sc, events_from_controls[waiting]);
    }
    else
    {
        call_game_event(events_from_keys[sc]);
    }
}

/*
 * Function:    call_game_event
 * ----------------------------
 *
 * Abstracts event-handling logic so that a game action can be called
 * without it being hard-coded into the keystroke.
 *
 * Arguments:
 *      enum GameEvent event: What type of event should happen.
 *
 * Effects:
 *      is_running
 *      current_block
 *
 */

void call_game_event(enum game_event event)
{
    printf("Gamestate: %d\n", is_running);
    switch (event)
    {
        case GAMEEVENT_NONE:
            break;

        case GAMEEVENT_QUIT:
            // TODO: should this be a "quit" function?
            is_running = GAMESTATE_QUIT;
            break;

        case GAMEEVENT_LEFT:
            if ((is_running & GAMESTATE_UNPAUSED) == GAMESTATE_UNPAUSED)
            {
                debug_log(LOG_OUTPUT, "GAMEEVENT_LEFT");
                current_block = attempt_move(current_block, DIR_LEFT);
            }
            else if (is_running & GAMESTATE_IS_OPTIONS)
            {
                change_value_current_option(-1);
            }
            break;

        case GAMEEVENT_RIGHT:
            if ((is_running & GAMESTATE_UNPAUSED) == GAMESTATE_UNPAUSED)
            {
                debug_log(LOG_OUTPUT, "GAMEEVENT_RIGHT");
                current_block = attempt_move(current_block, DIR_RIGHT);
            }
            else if (is_running & GAMESTATE_IS_OPTIONS)
            {
                change_value_current_option(1);
            }
            break;

        case GAMEEVENT_UP:
            if ((is_running & GAMESTATE_UNPAUSED) == GAMESTATE_UNPAUSED)
            {
                current_block = attempt_rotate(current_block, 0);
            }
            else if ((is_running & GAMESTATE_IS_ACTIVE) == 0)
            {
                move_menu_item(-1);
            }
            break;

        case GAMEEVENT_DOWN:
            if ((is_running & GAMESTATE_UNPAUSED) == GAMESTATE_UNPAUSED)
            {
                if (block_is_equal(current_block,
                                   attempt_move(current_block,
                                   DIR_DOWN)) == 0)
                {
                    score_soft_drop();
                }
                current_block = attempt_move(current_block, DIR_DOWN);
            }
            else if ((is_running & GAMESTATE_IS_ACTIVE) == 0)
            {
                move_menu_item(1);
            }
            break;

        case GAMEEVENT_CCW:
            if ((is_running & GAMESTATE_UNPAUSED) == GAMESTATE_UNPAUSED)
            {
                current_block = attempt_rotate(current_block, 1);
            }
            break;

        case GAMEEVENT_CANCEL:
            if ((is_running & GAMESTATE_IS_ACTIVE) == GAMESTATE_IS_ACTIVE)
            {
                is_running ^= GAMESTATE_IS_UNPAUSED;
                if (is_running & GAMESTATE_IS_UNPAUSED)
                {
                    play_music();
                }
                else
                {
                    pause_music();
                }
            }
            else
            {
                current_menu = MENU_MAIN;
                is_running &= GAMESTATE_MAIN_MENU;
                waiting = -1;
            }
            break;

        case GAMEEVENT_CONFIRM:
            if ((is_running & GAMESTATE_UNPAUSED) == GAMESTATE_UNPAUSED)
            {
                hard_drop();
            }
            else if ((is_running & GAMESTATE_IS_ACTIVE) == 0)
            {
                select_item();
            }
            break;

        default:
            break;
    }
}
