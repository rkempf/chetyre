/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * events.h                                                           *
 **********************************************************************
 *                                                                    *
 * Contains logic to handle events and key presses and delegate them  *
 * to the appropriate locations.                                      *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Functions:                                                  *
 *      void handle_events(): Event handler. Polls for SDL_Events,    *
 *                            and if one is provided, delegates or    *
 *                            handles it.                             *
 *                                                                    *
 **********************************************************************/

#ifndef EVENTS_H
#define EVENTS_H

// Enumerated game events, so that they can be abstracted
// from the game's SDL events.
enum game_event
{
    GAMEEVENT_NONE = 0,
    GAMEEVENT_QUIT = 1,
    GAMEEVENT_LEFT = 2,
    GAMEEVENT_RIGHT = 3,
    GAMEEVENT_UP = 4,
    GAMEEVENT_DOWN = 5,
    GAMEEVENT_CCW = 6,
    GAMEEVENT_CANCEL = 7,
    GAMEEVENT_CONFIRM = 8,
    GAMEEVENT_MIN = GAMEEVENT_NONE,
    GAMEEVENT_MAX = GAMEEVENT_CONFIRM
};

void handle_events(void);

#endif
