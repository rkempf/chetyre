/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * music.h                                                            *
 **********************************************************************
 *                                                                    *
 * Governs music loading and playing.                                 *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      enum song active_song: Currently selected song.               *
 *                                                                    *
 * Public Functions:                                                  *
 *      int init_music(void):                                         *
 *          Initializes music handler.                                *
 *      void clean_audio(void):                                       *
 *          Cleans up music handler at end of game.                   *
 *      void play_music(void):                                        *
 *          Plays selected music.                                     *
 *      void pause_music(void):                                       *
 *          Pauses selected music.                                    *
 *      void stop_music(int fade):                                    *
 *          Stops playing music. If fade is true, fades out first.    *
 *      void switch_music(enum song new_song):                        *
 *          Switches currently active music to play to new_song.      *
 *      void set_music_volume(int level):                             *
 *          Sets music volume level.                                  *
 *                                                                    *
 **********************************************************************/

#ifndef MUSIC_H
#define MUSIC_H

enum audio_state
{
    AUD_PAUSED = 2,
    AUD_PLAYING = 1,
    AUD_SUCCESS = 0,
    AUD_NOT_LOADED = -4,
    AUD_INIT_FAIL = -8,
    AUD_LOAD_FAIL_KOR = -16,
    AUD_LOAD_FAIL_KAT = -32
};

enum song
{
    SONG_NONE = 0,
    SONG_KOROBEINIKI = 1,
    SONG_KATYUSHA = 2
};

int init_music(void);
void clean_audio(void);
void play_music(void);
void pause_music(void);
void stop_music(int fade);
void switch_music(enum song new_song);
void set_music_volume(int level);

extern enum song active_song;

#endif
