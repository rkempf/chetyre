/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * game.h                                                             *
 **********************************************************************
 *                                                                    *
 * Contains main game code.                                           *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      int is_running: Whether or not the game is running.           *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init(void):                                              *
 *          Initializes the game. Called on startup.                  *
 *      void update(void):                                            *
 *          Updates game logic.                                       *
 *      void clean(void):                                             *
 *          Cleans up when game is done. Called at end.               *
 *                                                                    *
 **********************************************************************/

#ifndef GAME_H
#define GAME_H

enum game_state
{
    GAMESTATE_QUIT = 0,

    GAMESTATE_IS_RUNNING = 128,
    GAMESTATE_IS_ACTIVE = 64,
    GAMESTATE_IS_UNPAUSED = 32,
    GAMESTATE_IS_GAMEOVER = 16,
    GAMESTATE_IS_OPTIONS = 8,
    GAMESTATE_IS_CONTROLS = 4,
    GAMESTATE_IS_HIGHSCORE = 2,

    GAMESTATE_RUNNING = GAMESTATE_IS_RUNNING,
    GAMESTATE_PAUSED = GAMESTATE_RUNNING | GAMESTATE_IS_ACTIVE,
    GAMESTATE_UNPAUSED = GAMESTATE_PAUSED | GAMESTATE_IS_UNPAUSED,
    GAMESTATE_GAMEOVER = GAMESTATE_UNPAUSED | GAMESTATE_IS_GAMEOVER,
    GAMESTATE_LOCKOUT = GAMESTATE_GAMEOVER,
    GAMESTATE_BLOCKOUT = GAMESTATE_GAMEOVER | 1,
    GAMESTATE_MAIN_MENU = GAMESTATE_RUNNING,
    GAMESTATE_OPTIONS = GAMESTATE_MAIN_MENU | GAMESTATE_IS_OPTIONS,
    GAMESTATE_CONTROLS = GAMESTATE_OPTIONS | GAMESTATE_IS_CONTROLS,
    GAMESTATE_HIGHSCORES = GAMESTATE_MAIN_MENU | GAMESTATE_IS_HIGHSCORE
};

extern enum game_state is_running;

void init(void);
void new_game(void);
void update(void);
void clean(void);

#endif
