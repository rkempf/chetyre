/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * field.c                                                            *
 **********************************************************************
 *                                                                    *
 * Contains code around the play region where blocks fall.            *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      enum tile_type field[][]: The tile grid where blocks fall.    *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_field(void):                                        *
 *          Initializes the field with base values.                   *
 *      void check_for_rows(void):                                    *
 *          Checks if any rows are complete.                          *
 *      void fprint_field(FILE *stream):                              *
 *          Prints field to a stream.                                 *
 *      void print_field(void):                                       *
 *          Prints field to stdout.                                   *
 *                                                                    *
 * Private Functions:                                                 *
 *      void clear_rows(int rows[4]):                                 *
 *          Clears (complete) rows.                                   *
 *                                                                    *
 **********************************************************************/

#include <stdio.h>
#include "field.h"
#include "score.h"

// TODO: add extra for a "buffer zone".

// The "game board", so to speak: where all of the tiles will fall into.
enum tile_type field[FIELD_WIDTH][FIELD_HEIGHT+BUFFER_HEIGHT];

void clear_rows(int rows[4]);

/*
 * Function:    init_field
 * -----------------------
 *
 * Sets up field into its initial state: empty.
 *
 * Effects:
 *      field
 *
 */

void init_field(void)
{
    int i = 0;
    int j = 0;

    for (i = 0; i < FIELD_WIDTH; i++)
    {
        for (j = 0; j < FIELD_HEIGHT+BUFFER_HEIGHT; j++)
        {
            field[i][j] = TILE_NONE;
        }
    }
}

/*
 * Function:    check_for_rows
 * ---------------------------
 *
 * Checks for any unbroken rows on the field, and if any exist,
 * sends call to score and clear them.
 *
 */

void check_for_rows(void)
{
    int i = 0;
    int j = 0;
    int full_row = 1;
    int num_rows = 0;
    int rows[4] = {-1, -1, -1, -1};

    printf("Check for rows.\n");

    for (j = 0; j < FIELD_HEIGHT+BUFFER_HEIGHT; j++)
    {
        full_row = 1;
        for (i = 0; i < FIELD_WIDTH; i++)
        {
            full_row &= (field[i][j] > 0);
        }
        if (full_row) rows[num_rows] = j;
        num_rows += full_row;
    }

    score_rows(num_rows);
    clear_rows(rows);
}

/*
 * Function:    clear_rows
 * -----------------------
 *
 * Clears rows at the given indices.
 *
 * Arguments:
 *      int rows[4]: An array of up to 4 rows to clear. -1 is no row.
 *
 * Effects:
 *      field
 *
 */

void clear_rows(int rows[4])
{
    int row = 0;
    int i = 0;
    int j = 0;

    // TODO: add in some sort of signifier of clearing

    for (row = 0; row < 4; row++)
    {
        if (rows[row] == -1) continue;
        for (i = 0; i < FIELD_WIDTH; i++)
        {
            field[i][row] = TILE_NONE;
            for (j = rows[row]; j > 0; j--)
            {
                field[i][j] = field[i][j-1];
            }
        }
    }
}

/*
 * Function:    fprint_field
 * -------------------------
 *
 * Prints field information to a stream. Useful for debugging.
 *
 */
void fprint_field(FILE *stream)
{
    int i = 0;
    int j = 0;

    for (j = 0; j < BUFFER_HEIGHT; j++)
    {
        for (i = 0; i < FIELD_WIDTH; i++)
        {
            fprintf(stream, "%d ", field[i][j]);
        }
        fprintf(stream, "\n");
    }
    fprintf(stream, "------------------------------\n");
    for (j = BUFFER_HEIGHT; j < FIELD_HEIGHT + BUFFER_HEIGHT; j++)
    {
        for (i = 0; i < FIELD_WIDTH; i++)
        {
            fprintf(stream, "%d ", field[i][j]);
        }
        fprintf(stream, "\n");
    }
}

/*
 * Function:    print_field
 * ------------------------
 *
 * Prints field information to stdout. Useful for debugging.
 *
 */
void print_field(void)
{
    fprint_field(stdout);
}
