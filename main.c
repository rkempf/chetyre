/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * main.c                                                             *
 **********************************************************************
 *                                                                    *
 * This file sets up the basic architecture for the game:             *
 * initialization, the main loop, and cleanup.                        *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Functions:                                                  *
 *      int main(int argc, char** argv): The main function.           *
 *                                                                    *
 **********************************************************************/

#include <stdint.h>
#include <SDL2/SDL.h>
#include "game.h"
#include "events.h"
#include "graphics.h"

/*
 * Function:    main
 * -----------------
 *
 * Main function for the game. Initializes, loops, cleans up. Inside
 * the loop: handles events, updates, renders.
 *
 * Arguments:
 *      int argc: Count of arguments (including filename) from terminal.
 *      char** argv: Array of strings with the arguments.
 *
 * Returns:
 *      int (): 0 if everything is successful.
 *
 */

int main(int argc, char** argv)
{
    const int FPS = 60;
    const int frame_delay = 1000 / FPS;
    uint32_t frame_start;
    int frame_time;

    // startup
    init();

    // game loop
    while (is_running)
    {
        frame_start = SDL_GetTicks();

        // handle user input
        handle_events();
        // update game + objects (pipe through game?)
        update();
        // render display (if appropriate)
        render();

        frame_time = SDL_GetTicks() - frame_start;
        if (frame_delay > frame_time)
        {
            SDL_Delay(frame_delay - frame_time);
        }
    }

    // shutdown
    clean();

    return 0;
}
