/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * menu.c                                                             *
 **********************************************************************
 *                                                                    *
 * Governs menu logic for all menus.                                  *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      int *_menu_selected: Current selection for appropriate menu.  *
 *                                                                    *
 * Public Functions:                                                  *
 *      void move_menu_item(enum menu_type menu, int change):         *
 *          Changes selected menu item by change.                     *
 *      void select_item(void):                                       *
 *          Selects the given menu item.                              *
 *                                                                    *
 **********************************************************************/

#include <stdio.h>
#include "menu.h"
#include "game.h"
#include "options.h"
#include "controls.h"
#include "score.h"

#define WRAP 0

int main_menu_selected;
int options_menu_selected;
int controls_menu_selected;
int pause_menu_selected;
int gameover_menu_selected;

enum menu_type current_menu;

/*
 * Function:    move_menu_item
 * ---------------------------
 *
 * Changes the selected menu item by change.
 *
 * Effects:
 *      *_menu_selected
 *
 */
void move_menu_item(int change)
{
    switch (current_menu)
    {
        case MENU_MAIN:
            if (WRAP)
            {
                main_menu_selected = (main_menu_selected + change) %
                                     (MAINMENU_MAX + 1);
            }
            else
            {
                main_menu_selected = main_menu_selected + change;
                if (main_menu_selected > MAINMENU_MAX)
                {
                    main_menu_selected = MAINMENU_MAX;
                }
                if (main_menu_selected < MAINMENU_MIN)
                {
                    main_menu_selected = MAINMENU_MIN;
                }
            }
            printf("Main menu: %d\n", main_menu_selected);
            break;

        case MENU_OPTIONS:
            change_current_option(change);
            break;

        case MENU_CONTROLS:
            change_current_control(change);
            break;

        default:
            break;
    }
}

/*
 * Function:    select_item
 * ------------------------
 *
 * Performs the requisite action depending on the selection
 * the player has made on the menu.
 *
 */

void select_item(void)
{
    if (current_menu == MENU_MAIN)
    {
        if (main_menu_selected == MAINMENU_NEWGAME)
        {
            current_menu = MENU_NONE;
            new_game();
        }
        else if (main_menu_selected == MAINMENU_HIGHSCORES)
        {
            current_menu = MENU_HIGHSCORES;
            is_running = GAMESTATE_HIGHSCORES;
        }
        else if (main_menu_selected == MAINMENU_OPTIONS)
        {
            current_menu = MENU_OPTIONS;
            is_running = GAMESTATE_OPTIONS;
            setup_options_menu();
        }
        else if (main_menu_selected == MAINMENU_CONTROLS)
        {
            current_menu = MENU_CONTROLS;
            is_running = GAMESTATE_CONTROLS;
            init_controls_menu();
        }
        else if (main_menu_selected == MAINMENU_QUIT)
        {
            is_running = GAMESTATE_QUIT;
        }
    }
    else if (current_menu == MENU_OPTIONS)
    {
        if (options_menu_selected == OPTION_CLEARHIGHSCORES)
        {
            clear_high_scores();
        }
        else if (options_menu_selected == OPTION_BACKTOMAINMENU)
        {
            is_running = GAMESTATE_MAIN_MENU;
            current_menu = MENU_MAIN;
        }
    }
    else if (current_menu == MENU_CONTROLS)
    {
        if (controls_menu_selected == CONTROLS_RESET)
        {
            set_key_defaults();
            controls_menu_selected = CONTROLS_RESET;
        }
        else if (controls_menu_selected == CONTROLS_BACKTOMAINMENU)
        {
            is_running = GAMESTATE_MAIN_MENU;
            current_menu = MENU_MAIN;
            waiting = -1;
        }
        else
        {
            set_current_option_waiting();
        }
    }
}
