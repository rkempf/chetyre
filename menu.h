/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * menu.h                                                             *
 **********************************************************************
 *                                                                    *
 * Governs menu logic for all menus.                                  *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      int *_menu_selected: Current selection for appropriate menu.  *
 *                                                                    *
 * Public Functions:                                                  *
 *      void move_menu_item(enum menu_type menu, int change):         *
 *          Changes selected menu item by change.                     *
 *      void select_item(void):                                       *
 *          Selects the given menu item.                              *
 *                                                                    *
 **********************************************************************/

#ifndef MENU_H
#define MENU_H

enum menu_type
{
    MENU_NONE,
    MENU_MAIN,
    MENU_OPTIONS,
    MENU_CONTROLS,
    MENU_HIGHSCORES,
    MENU_PAUSE,
    MENU_GAMEOVER
};

enum main_menu
{
    MAINMENU_NEWGAME,
    MAINMENU_HIGHSCORES,
    MAINMENU_OPTIONS,
    MAINMENU_CONTROLS,
    MAINMENU_QUIT,
    MAINMENU_MIN = MAINMENU_NEWGAME,
    MAINMENU_MAX = MAINMENU_QUIT
};

extern int main_menu_selected;
extern int options_menu_selected;
extern int controls_menu_selected;
extern int pause_menu_selected;
extern int gameover_menu_selected;

extern enum menu_type current_menu;

void init_menus(void);
void move_menu_item(int change);
void select_item(void);

#endif
