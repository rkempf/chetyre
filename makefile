CC=gcc
HEADERS=$(wildcard *.h)
LIBS=-lSDL2 -lSDL2_image -lm -lSDL2_mixer
CFLAGS=-Wall -Wextra -Wstrict-prototypes -pedantic -std=c99
SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)
TXTS=highscores.txt options.txt keys.txt

chetyre: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o chetyre $(LIBS)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@ $(LIBS)

clean:
	rm -f *.o chetyre $(TXTS); touch $(TXTS)
