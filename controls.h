/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * controls.h                                                         *
 **********************************************************************
 *                                                                    *
 * Contains code dealing with setting keyboard controls.              *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      enum game_event events_from_keys[NUM_KEYS]:                   *
 *          Lookup from SDL_Scancodes to Game Events.                 *
 *      SDL_Scancode keys_from_events[GAMEEVENT_MAX+1]                *
 *          Lookup from Game Events to SDL_Scancodes.                 *
 *      char *strings_from_keys[NUM_KEYS]                             *
 *          Lookup from SDL_Scancodes to printable strings.           *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_keys(void):                                         *
 *          Sets up key-binding environment and initial keybinds.     *
 *      void set_key_command(SDL_Scancode sc, enum game_event ge):    *
 *          Links a key to an event and vice-versa.                   *
 *      void load_key_config(void):                                   *
 *          Loads key config information from a file.                 *
 *      void save_key_config(void):                                   *
 *          Saves key config information to a file.                   *
 *      void change_current_control(int direction):                   *
 *          Changes the selected control in a given direction.        *
 *      void set_current_option_waiting(void):                        *
 *          Sets the current control to await a new key value.        *
 *      void init_controls_menu(void):                                *
 *          Initializes the controls menu.                            *
 *      void set_key_defaults(void):                                  *
 *          Sets or resets keybinding defaults.                       *
 *                                                                    *
 **********************************************************************/

#ifndef CONTROLS_H
#define CONTROLS_H

#include <SDL2/SDL.h>
#include <wchar.h>
#include "events.h"

#define NUM_KEYS 285

enum controls
{
    CONTROLS_NONE = -1,
    CONTROLS_QUIT,
    CONTROLS_LEFT,
    CONTROLS_RIGHT,
    CONTROLS_UP,
    CONTROLS_DOWN,
    CONTROLS_CCW,
    CONTROLS_CANCEL,
    CONTROLS_CONFIRM,
    CONTROLS_RESET = CONTROLS_CONFIRM + 4,
    CONTROLS_BACKTOMAINMENU,
    CONTROLS_MIN = CONTROLS_QUIT,
    CONTROLS_MAX = CONTROLS_BACKTOMAINMENU
};

extern enum game_event events_from_keys[NUM_KEYS];
extern SDL_Scancode keys_from_events[GAMEEVENT_MAX+1];
extern wchar_t *strings_from_keys[NUM_KEYS];
extern int strlen_from_keys[NUM_KEYS];
extern enum game_event events_from_controls[CONTROLS_MAX+1];
extern int waiting;

void init_keys(void);
void set_key_command(SDL_Scancode sc, enum game_event ge);
void load_key_config(void);
void save_key_config(void);
void change_current_control(int direction);
void set_current_option_waiting(void);
void init_controls_menu(void);
void set_key_defaults(void);

#endif
