/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019-2020 Ragnar Kempf, all rights reserved.    *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * game.c                                                             *
 **********************************************************************
 *                                                                    *
 * Contains main game code.                                           *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      int is_running: Whether or not the game is running.           *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init(void):                                              *
 *          Initializes the game. Called on startup.                  *
 *      void update(void):                                            *
 *          Updates game logic.                                       *
 *      void clean(void):                                             *
 *          Cleans up when game is done. Called at end.               *
 *                                                                    *
 **********************************************************************/

#include <stdio.h>
#include <SDL2/SDL.h>
#include "game.h"
#include "log.h"
#include "graphics.h"
#include "field.h"
#include "block.h"
#include "score.h"
#include "events.h"
#include "menu.h"
#include "music.h"
#include "options.h"
#include "controls.h"

// Whether or not the game is currently running. Note that this represents
// the game/program as a whole, not any individual game/session.
enum game_state is_running;

/*
 * Function:    init
 * -----------------
 *
 * Sets everything up. Mainly just calls other init functions, including
 * initializing SDL.
 *
 * Effects:
 *      is_running
 *
 */

void init(void)
{
    int success = 0;

    if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        debug_log(LOG_OUTPUT, "SDL Initialized!");
        init_options();
        success |= init_graphics();
        init_keys();
        init_music();
        load_high_scores("highscores.txt");
        current_menu = MENU_MAIN;
        if (!success) is_running = GAMESTATE_MAIN_MENU;
    }
    else
    {
        debug_log(LOG_CRITICAL, "Failed to initialize SDL.");
        is_running = GAMESTATE_QUIT;
    }
}

/*
 * Function:    new_game
 * ---------------------
 *
 * Creates a new game.
 *
 * Effects:
 *      is_running
 *
 */

void new_game(void)
{
    init_field();
    init_block();
    init_score();
    is_running = GAMESTATE_UNPAUSED;
}

/*
 * Function:    update
 * -------------------
 *
 * Updates game. Calls any update functions.
 *
 */

void update(void)
{
    if (is_running & GAMESTATE_IS_GAMEOVER)
    {
        printf("Gameover: %d\n", is_running);
        add_high_score(current_score);
        init_score();
        is_running = GAMESTATE_MAIN_MENU;
        current_menu = MENU_MAIN;
    }
    else if ((is_running & GAMESTATE_UNPAUSED) == GAMESTATE_UNPAUSED)
    {
        update_block();
    }
}

/*
 * Function:    clean
 * ------------------
 *
 * Cleans up game upon closing. Calls cleanup functions, including
 * SDL_Quit.
 *
 */

void clean(void)
{
    clean_graphics();
    clean_audio();
    printf("Current score: %d\n", current_score);
    if (current_score > 0)
    {
        printf("Adding %d to high scores.\n", current_score);
        add_high_score(current_score);
    }
    save_high_scores("highscores.txt");
    clean_high_scores();
    save_options();
    save_key_config();
    SDL_Quit();
    debug_log(LOG_OUTPUT, "Game cleaned.");
}
