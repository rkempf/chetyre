/**********************************************************************
 * CHETYRE                                                            *
 **********************************************************************
 *                                                                    *
 * Chetyre is a game of falling blocks. Blocks fall in sets of four,  *
 * in each of the seven distinct configurations - rotations excepted, *
 * but reflections not excepted. The rule of the game is to form      *
 * unbroken rows, and by doing so prevent the blocks from stacking to *
 * the top of the field.                                              *
 *                                                                    *
 * Code copyright (c) 2019 Ragnar Kempf, all rights reserved.         *
 *                                                                    *
 **********************************************************************/

/**********************************************************************
 * events.h                                                           *
 **********************************************************************
 *                                                                    *
 * Contains logic to handle events and key presses and delegate them  *
 * to the appropriate locations.                                      *
 *                                                                    *
 **********************************************************************
 *                                                                    *
 * Public Variables:                                                  *
 *      struct block current_block: The currently active block.       *
 *      struct block next_block: The next block in the queue.         *
 *                                                                    *
 * Public Functions:                                                  *
 *      void init_block(void):                                        *
 *          Initializes the block system.                             *
 *      void update_block(void):                                      *
 *          Updates the current block.                                *
 *      struct block get_block_final_dest(struct block current):      *
 *          Gets the final destination for the current block.         *
 *      struct block attempt_move(struct block curr,                  *
 *                                enum direction dir):                *
 *          Attempts move and returns result.                         *
 *      struct block attempt_rotate(struct block block, int ccw):     *
 *          Attempts a rotation and returns result.                   *
 *      int block_is_equal(struct block block_1,                      *
 *                         struct block block_2):                     *
 *          Checks whether two blocks are equal or not.               *
 *      void hard_drop(void):                                         *
 *          Causes current block to fall immediately.                 *
 *                                                                    *
 **********************************************************************/

#ifndef BLOCK_H
#define BLOCK_H

#include "field.h"

enum orientation
{
    ORIENT_NORTH,
    ORIENT_EAST,
    ORIENT_SOUTH,
    ORIENT_WEST
};

enum direction
{
    DIR_DOWN,
    DIR_LEFT,
    DIR_RIGHT
};

struct block
{
    enum tile_type type;
    enum orientation orientation;
    int x[4];
    int y[4];
};

extern struct block current_block;
extern struct block next_block;

void init_block(void);
void update_block(void);
struct block get_block_final_dest(struct block current);
struct block attempt_move(struct block curr, enum direction dir);
struct block attempt_rotate(struct block block, int ccw);
int block_is_equal(struct block block_1, struct block block_2);
void hard_drop(void);

#endif
